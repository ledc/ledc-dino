# ledc-dino

ledc-dino (Discovery of Inconsistencies and Outliers) is a small repo that bundles algorithms for data-driven discovery of errors in a dataset.
It is part of the ledc framework and builds on top of the [ledc-sigma](https://gitlab.com/ledc/ledc-sigma) module.

## Why
Finding errors in a database can be a tedious task.
A popular approach to tackle the problem is by composing a set of constraints or rules to which data must adhere. 
Finding errors is then done by finding violations of those rules.
When data gets big (in terms of number of rows and number of columns), compiling a sufficiently large set of rules to cover all errors becomes tedious as well.

## What
This repository is part of ledc, which is short for *Lightweight Engine for Data quality Control*. 
It is a modular framework designed for data quality monitoring and improvement in several different ways.
The main features of ledc-dino are the following:

* **Constraint discovery**: ledc-dino provides several algorithms of proposing constraints for a given instance of data.
All these constraints are instances of sigma rules as defined in [ledc-sigma](https://gitlab.com/ledc/ledc-sigma).
In particular, this means that all constraints found, can be repaired by algorithms for [repairing](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/repair.md) violations of sigma-rules.

* **Outlier detection**: ledc-dino also implements some well-known algorithms for outlier detection.
These algorithms focus on finding suspicious and/or erroneous objects directly, rather than proposing constraints.
 
## Getting started

As all current and future components of ledc, ledc-dino is written in Java.
To run it, you require Java 17 or higher and Maven.
Just pull the code from this repository and build it with Maven.
Alternatively, you can have a look at the registry to download an executable JAR file.
Documentation can be found [here](docs/index.md).

## References

Below are listed some of the publications where the algorithms present in ledc-dino where conceived.

* Antoon Bronselaer, Toon Boeckling and Filip Pattyn, Dynamic repair of categorical data with edit rules *Expert Systems With Applications*, vol. 201, p. 117-132, (**2022**)

* Milan Peelman, Antoon Bronselaer and Guy De Tré, Discovery of pairwise ordinal edit rules *Joint Proceedings of the 19th World Congress of the International Fuzzy Systems Association (IFSA), the 12th Conference of the European Society for Fuzzy Logic and Technology (EUSFLAT), and the 11th International Summer School on Aggregation Operators (AGOP)*, p. 109-116, (**2021**)

* Toon Boeckling, Antoon Bronselaer and Guy De Tré, Mining data quality rules based on *T*-dependence, *Proceedings of the 11th Conference of the European Society for Fuzzy Logic and Technology (EUSFLAT 2019)*, p.184-191, (**2019**) 
