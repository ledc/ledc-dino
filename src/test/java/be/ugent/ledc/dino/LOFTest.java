package be.ugent.ledc.dino;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.BruteForceNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.lof.LOFDetector;
import java.util.Map;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author abronsel
 */
public class LOFTest
{
    private final Dataset data = new SimpleDataset();
    
    @Before
    public void setUp()
    {
        data.addDataObject(new DataObject().setDouble("x", 0.93).setDouble("y", 1.22).setInteger("id", 1));
        data.addDataObject(new DataObject().setDouble("x", 1.32).setDouble("y", 2.34).setInteger("id", 2));
        data.addDataObject(new DataObject().setDouble("x", 1.80).setDouble("y", 1.55).setInteger("id", 3));
        data.addDataObject(new DataObject().setDouble("x", 1.80).setDouble("y", 0.71).setInteger("id", 4));
        data.addDataObject(new DataObject().setDouble("x", 2.28).setDouble("y", 2.64).setInteger("id", 5));
        data.addDataObject(new DataObject().setDouble("x", 2.65).setDouble("y", 1.07).setInteger("id", 6));
        data.addDataObject(new DataObject().setDouble("x", 3.43).setDouble("y", 1.85).setInteger("id", 7));
        data.addDataObject(new DataObject().setDouble("x", 4.58).setDouble("y", 2.94).setInteger("id", 8));
    }


    @Test
    public void lofScoreTest()
    {
        LOFDetector lofDetector = new LOFDetector(
            new BruteForceNeighbourSearch(SetOperations.set("x","y"), data),
            3,
            1.2);
        
        Map<DataObject, Double> outliers = lofDetector.findOutliers(data, "x", "y");
        
        assertTrue(outliers.size() == 1);
        assertTrue(outliers
            .keySet()
            .contains(new DataObject()
                .setDouble("x", 4.58)
                .setDouble("y", 2.94)
                .setInteger("id", 8)));
    }
}
