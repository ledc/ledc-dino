package be.ugent.ledc.dino;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.dino.outlierdetection.hbos.HBOSDetector;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

public class HBOSTest
{
    private static ContractedDataset dataset;

    @BeforeClass
    public static void init()
    {
        dataset = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("x", SigmaContractorFactory.INTEGER)
            .addContractor("y", SigmaContractorFactory.INTEGER)
            .build()
        );
        
        //Cluster 1
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 1));
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 2));
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 3));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 1));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 2));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 3));
       
        //Cluster 2
        dataset.addDataObject(new DataObject().set("x", 10).set("y", 13));
        dataset.addDataObject(new DataObject().set("x", 11).set("y", 11));
        dataset.addDataObject(new DataObject().set("x", 11).set("y", 12));
        dataset.addDataObject(new DataObject().set("x", 11).set("y", 13));
        dataset.addDataObject(new DataObject().set("x", 12).set("y", 10));
        dataset.addDataObject(new DataObject().set("x", 12).set("y", 11));
        dataset.addDataObject(new DataObject().set("x", 12).set("y", 12));
        dataset.addDataObject(new DataObject().set("x", 12).set("y", 13));
        
        //Outlier
        dataset.addDataObject(new DataObject().set("x", 5).set("y", 5));
    }
    
    @Test
    public void hbosTest()
    {
        Map<DataObject, Double> outliers = new HBOSDetector(3.0, false)
        .findOutliers(dataset, "x", "y");
        
        DataObject outlier = new DataObject().set("x", 5).set("y", 5);
        
        assertEquals(1, outliers.size());
        assertTrue(outliers.keySet().contains(outlier));
        assertEquals(Math.log(8) + Math.log(6), outliers.get(outlier), Double.MIN_VALUE);
    }
}
