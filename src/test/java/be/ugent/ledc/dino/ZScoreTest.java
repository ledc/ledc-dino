package be.ugent.ledc.dino;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.dino.outlierdetection.zscore.ZScoreOutlierDetector;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

public class ZScoreTest
{
    @Test
    public void zScoreUnaryTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        for(int i=0; i<50; i++)
            dataset
                .addDataObject(new DataObject()
                    .set("a", 1));
        
        dataset
            .addDataObject(new DataObject()
                .set("a", 10));
        
        Map<DataObject, Double> outliers = new ZScoreOutlierDetector()
            .findOutliers(dataset, "a");
        
        assertEquals(1, outliers.size());
        assertTrue(outliers.containsKey(new DataObject().set("a", 10)));
    }
    
    @Test
    public void zScoreUnaryWithNullsTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        for(int i=0; i<50; i++)
            dataset
                .addDataObject(new DataObject()
                    .set("a", 1));
        
        for(int i=0; i<5; i++)
            dataset
                .addDataObject(new DataObject()
                    .set("a", null));
        
        dataset
            .addDataObject(new DataObject()
                .set("a", 10));
        
        Map<DataObject, Double> outliers = new ZScoreOutlierDetector()
            .findOutliers(dataset, "a");
        
        assertEquals(1, outliers.size());
        assertTrue(outliers.containsKey(new DataObject().set("a", 10)));
    }
    
    @Test
    public void zScoreBinaryTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        for(int i=0; i<25; i++)
            dataset
                .addDataObject(new DataObject()
                    .set("a", 1)
                    .set("b", 10)
                );
        
        for(int i=0; i<25; i++)
            dataset
                .addDataObject(new DataObject()
                    .set("a", 1)
                    .set("b", 30)
                );
        
        dataset
            .addDataObject(new DataObject()
                .set("a", 5)
                .set("b", 60)
            );
        
        Map<DataObject, Double> outliers = new ZScoreOutlierDetector()
            .findOutliers(dataset, "a", "b");

        assertEquals(1, outliers.size());
        assertTrue(outliers.containsKey(new DataObject()
                .set("a", 5)
                .set("b", 60)
        ));
        assertTrue(outliers.get(new DataObject()
                .set("a", 5)
                .set("b", 60)) > 7
        );
    }
    
    @Test
    public void zScoreUniformityTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        for(int i=0; i<50; i++)
            dataset
                .addDataObject(new DataObject()
                    .set("a", 1));
        
        Map<DataObject, Double> outliers = new ZScoreOutlierDetector()
            .findOutliers(dataset, "a");

        assertTrue(outliers.isEmpty());
    }
}
