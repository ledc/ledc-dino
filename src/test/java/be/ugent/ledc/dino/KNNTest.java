package be.ugent.ledc.dino;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.BruteForceNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.knn.KNNDetector;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.util.Map;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class KNNTest
{
    private static ContractedDataset dataset;

    @BeforeClass
    public static void init()
    {
        dataset = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("x", SigmaContractorFactory.INTEGER)
            .addContractor("y", SigmaContractorFactory.INTEGER)
            .build()
        );
        
        //Cluster 1
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 1));
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 2));
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 3));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 1));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 2));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 3));
       
        //Cluster 2
        dataset.addDataObject(new DataObject().set("x", 7).set("y", 10));
        dataset.addDataObject(new DataObject().set("x", 8).set("y", 8));
        dataset.addDataObject(new DataObject().set("x", 8).set("y", 9));
        dataset.addDataObject(new DataObject().set("x", 8).set("y", 10));
        dataset.addDataObject(new DataObject().set("x", 9).set("y", 7));
        dataset.addDataObject(new DataObject().set("x", 9).set("y", 8));
        dataset.addDataObject(new DataObject().set("x", 9).set("y", 9));
        dataset.addDataObject(new DataObject().set("x", 9).set("y", 10));
        
        //Outlier
        dataset.addDataObject(new DataObject().set("x", 5).set("y", 5));
    }

    @Test
    public void kNNTest1()
    {
        Map<DataObject, Double> outliers = new KNNDetector(
            new BruteForceNeighbourSearch(SetOperations.set("x", "y"), dataset),
            1,
            3.0,
            DataObjectMetrics.EUCLIDEAN)
        .findOutliers(dataset, "x", "y");
        
        DataObject outlier = new DataObject()
                .set("x", 5)
                .set("y", 5);
        
        assertEquals(1, outliers.size());
        assertTrue(outliers.keySet().contains(outlier));
        assertEquals(Math.sqrt(13), outliers.get(outlier), Double.MIN_VALUE);
    }
    
    @Test
    public void kNNTest2()
    {
        Map<DataObject, Double> outliers = new KNNDetector(
            new BruteForceNeighbourSearch(SetOperations.set("x", "y"), dataset),
            2,
            3.0,
            DataObjectMetrics.EUCLIDEAN)
        .findOutliers(dataset, "x", "y");
        
        DataObject outlier = new DataObject()
                .set("x", 5)
                .set("y", 5);
        
        assertEquals(1, outliers.size());
        assertTrue(outliers.keySet().contains(outlier));
        assertEquals(Math.sqrt(18), outliers.get(outlier), Double.MIN_VALUE);
    }
}
