package be.ugent.ledc.dino;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.dino.rulemining.association.ConfidenceMiner;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Assert;
import org.junit.Test;

public class AssociationMinerTest
{
    @Test
    public void assocationMinerTest() throws ParseException
    {
        ContractedDataset dataset = new ContractedDataset(
            new Contract.ContractBuilder()
                .addContractor("x", TypeContractorFactory.STRING)
                .addContractor("y", TypeContractorFactory.STRING)
                .addContractor("id", TypeContractorFactory.INTEGER)
                .build()
        );
        
        for(int i=0;i<500; i++)
            dataset
                .addDataObject(
                    new DataObject().
                        set("x", "a")
                        .set("y", "b")
                        .set("id", i));
        
        for(int i=0;i<500; i++)
            dataset
                .addDataObject(
                    new DataObject().
                        set("x", "c")
                        .set("y", "d")
                        .set("id", i + 500));
            
        dataset
            .addDataObject(
                new DataObject().
                    set("x", "a")
                    .set("y", "d")
                    .set("id", 10000));

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("x", SigmaContractorFactory.STRING);
        contractors.put("y", SigmaContractorFactory.STRING);
        
        Set<SigmaRule> sigmaRules = Stream.of(
            SigmaRuleParser.parseSigmaRule("x in {'a', 'c'}", contractors),
            SigmaRuleParser.parseSigmaRule("y in {'b', 'd'}", contractors),
            SigmaRuleParser.parseSigmaRule("not x == 'a' & y != 'b'", contractors),
            SigmaRuleParser.parseSigmaRule("not y == 'b' & x != 'a'", contractors),
            SigmaRuleParser.parseSigmaRule("not x == 'c' & y != 'd'", contractors),
            SigmaRuleParser.parseSigmaRule("not y == 'd' & x != 'c'", contractors)
        )
        .collect(Collectors.toSet());
        
        SigmaRuleset expected = new SigmaRuleset(contractors, sigmaRules);
        
        //Compute
        SigmaRuleset rules = new ConfidenceMiner.ConfidenceMinerBuilder(0.4, 0.98)
                .build()
                .findRules(dataset, "x", "y");       
        
        Assert.assertEquals(expected, rules);
    }
}
