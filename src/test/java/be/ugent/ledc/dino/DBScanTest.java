package be.ugent.ledc.dino;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.dino.outlierdetection.BruteForceNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.TreeNeighbourSearch;
import be.ugent.ledc.dino.outlierdetection.dbscan.DBScanDetector;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

public class DBScanTest
{
    private static ContractedDataset dataset;

    @BeforeClass
    public static void init()
    {
        dataset = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("x", SigmaContractorFactory.INTEGER)
            .addContractor("y", SigmaContractorFactory.INTEGER)
            .build()
        );
        
        //Cluster 1
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 1));
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 2));
        dataset.addDataObject(new DataObject().set("x", 1).set("y", 3));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 1));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 2));
        dataset.addDataObject(new DataObject().set("x", 2).set("y", 3));
       
        //Cluster 2
        dataset.addDataObject(new DataObject().set("x", 7).set("y", 10));
        dataset.addDataObject(new DataObject().set("x", 8).set("y", 8));
        dataset.addDataObject(new DataObject().set("x", 8).set("y", 9));
        dataset.addDataObject(new DataObject().set("x", 8).set("y", 10));
        dataset.addDataObject(new DataObject().set("x", 9).set("y", 7));
        dataset.addDataObject(new DataObject().set("x", 9).set("y", 8));
        dataset.addDataObject(new DataObject().set("x", 9).set("y", 9));
        dataset.addDataObject(new DataObject().set("x", 9).set("y", 10));
        
        //Outlier
        dataset.addDataObject(new DataObject().set("x", 5).set("y", 5));
    }
    
    @Test
    public void dbScanTest()
    {
        Map<DataObject, Boolean> outliers = new DBScanDetector(
            new BruteForceNeighbourSearch(SetOperations.set("x", "y"), dataset),
            2,
            3,
            DataObjectMetrics.EUCLIDEAN)
        .findOutliers(dataset, "x", "y");
        
        assertEquals(1, outliers.size());
        assertTrue(outliers.keySet().contains(new DataObject().set("x", 5).set("y", 5)));
    }
    
    @Test
    public void dbScanKdTreeTest()
    {
        Map<DataObject, Boolean> outliers = new DBScanDetector(
            new TreeNeighbourSearch<>(
                SetOperations.set("x", "y"),
                dataset,
                SigmaContractorFactory.INTEGER
            ),
            2,
            3,
            DataObjectMetrics.EUCLIDEAN)
        .findOutliers(dataset, "x", "y");
        
        assertEquals(1, outliers.size());
        assertTrue(outliers.keySet().contains(new DataObject().set("x", 5).set("y", 5)));
    }
    
    @Test
    public void dbScanIgnoreAttributeTest()
    {
        SimpleDataset extended = new SimpleDataset();
             
        //Cluster 1
        extended.addDataObject(new DataObject().set("x", 1).set("y", 1).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 1).set("y", 2).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 1).set("y", 3).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 2).set("y", 1).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 2).set("y", 2).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 2).set("y", 3).set("z", "ignore"));
       
        //Cluster 2
        extended.addDataObject(new DataObject().set("x", 7).set("y", 10).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 8).set("y", 8).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 8).set("y", 9).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 8).set("y", 10).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 9).set("y", 7).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 9).set("y", 8).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 9).set("y", 9).set("z", "ignore"));
        extended.addDataObject(new DataObject().set("x", 9).set("y", 10).set("z", "ignore"));
        
        //Outlier
        extended.addDataObject(new DataObject().set("x", 5).set("y", 5).set("z", "ignore"));
        
        Map<DataObject, Boolean> outliers = new DBScanDetector(
            new BruteForceNeighbourSearch(SetOperations.set("x", "y"), extended),
            2,
            3,
            DataObjectMetrics.EUCLIDEAN)
        .findOutliers(extended, "x", "y");
        
        assertEquals(1, outliers.size());
        assertTrue(outliers.keySet().contains(new DataObject().set("x", 5).set("y", 5).set("z", "ignore")));
    }
}
