package be.ugent.ledc.dino;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.dino.rulemining.ordinal.OrdinalBinaryMiner;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Assert;
import org.junit.Test;

public class OrdinalBinaryMinerTest
{
    @Test
    public void ordinalMiningTest() throws ParseException
    {
         ContractedDataset dataset = new ContractedDataset(
            new Contract.ContractBuilder()
                .addContractor("x", SigmaContractorFactory.INTEGER)
                .addContractor("y", SigmaContractorFactory.BIGDECIMAL_ONE_DECIMAL)
                .addContractor("id", TypeContractorFactory.STRING)
                .build()
        );
 
        for(int i=0;i<500; i++)
            dataset
                .addDataObject(
                    new DataObject().
                        set("x", 1)
                        .setBigDecimal("y", new BigDecimal("2.0"))
                        .set("id", "" + i));
        
        for(int i=0;i<500; i++)
            dataset
                .addDataObject(
                    new DataObject().
                        set("x", 10)
                        .set("y", new BigDecimal("20.0"))
                        .set("id", "" + (i + 500)));
            
        dataset
            .addDataObject(
                new DataObject().
                    set("x", 1)
                    .set("y", new BigDecimal("10.0"))
                    .set("id", "" + 10000));

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("x", SigmaContractorFactory.INTEGER);
        contractors.put("y", SigmaContractorFactory.BIGDECIMAL_ONE_DECIMAL);
        
        Set<SigmaRule> sigmaRules = Stream.of(
            SigmaRuleParser.parseSigmaRule("x >= 1", contractors),
            SigmaRuleParser.parseSigmaRule("x <= 10", contractors),
            SigmaRuleParser.parseSigmaRule("y >= 2.0", contractors),
            SigmaRuleParser.parseSigmaRule("y <= 20.0", contractors),
            SigmaRuleParser.parseSigmaRule("not x <= 1 & y >= 10.0 ", contractors)
        )
        .collect(Collectors.toSet());
        
        SigmaRuleset expected = new SigmaRuleset(contractors, sigmaRules);
        
        //Compute
        SigmaRuleset rules = new OrdinalBinaryMiner(0.01)
                .findRules(dataset, "x", "y");       
        
        Assert.assertEquals(expected, rules);
    }
}
