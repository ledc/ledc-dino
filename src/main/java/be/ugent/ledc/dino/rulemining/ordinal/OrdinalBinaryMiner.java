package be.ugent.ledc.dino.rulemining.ordinal;

import be.ugent.ledc.dino.RuleMiner;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleFactory;

public class OrdinalBinaryMiner implements RuleMiner<ContractedDataset>
{
    private final double lambda;
    
    private final Map<String, TreeMap<Comparable, Integer>> countMap = new HashMap<>();
    private final Map<String, TreeMap<Comparable, Integer>> cumulMap = new HashMap<>();
            
    public OrdinalBinaryMiner(double lambda)
    {
        this.lambda = lambda;
    }
    
    public OrdinalBinaryMiner()
    {
        this(0.001);
    }
    
    @Override
    public SigmaRuleset findRules(ContractedDataset dataset, Set<String> attributes)
    {
        //Keep a list of attributes
        List<String> attributeList = new ArrayList<>(attributes);

        //Clear maps
        this.countMap.clear();
        this.cumulMap.clear();
        
        //Init the maps
        for(String a: attributeList)
        {
            countMap.put(a, new TreeMap<>());
            cumulMap.put(a, new TreeMap<>());
        }
        
        //Rules
        Set<SigmaRule> rules = new HashSet<>();
        
        //First check if rules can be found
        double d = dataset.getSize();
        
        if(this.lambda < (4.0 * d) / Math.pow(d-1.0, 2.0))
        {
            System.out.println("Warning: the value lambda=" + this.lambda + " is too low to find errors in a monotone dataset with size " + dataset.getSize());
            return new SigmaRuleset(new HashMap<>(), rules);
        }
        
        //Initialize the count map
        for(DataObject o: dataset.getDataObjects())
        {
            for(String a: attributes)
            {
                OrdinalContractor<?> contractor = (OrdinalContractor) dataset
                    .getContract()
                    .getAttributeContract(a);
                
                if(o.get(a) != null)
                    countMap.get(a).merge((Comparable)contractor.getFromDataObject(o, a), 1, Integer::sum);                
            }
        }
        
        //Initialize the cumulative map
        for(String a: attributeList)
        {
            int cumul = 0;
            for(Comparable v: countMap.get(a).keySet())
            {
                cumul += countMap.get(a).get(v);
                cumulMap.get(a).put(v,  cumul);
            }
        }
        
        //Now we sort the attributes depending on the number of distinct values
        Collections.sort(attributeList, Comparator.comparing(a -> countMap.get(a).size()));
        
        //Iterate over the sorted attributes
        for(int i = 0; i<attributeList.size(); i++)
        {
            //This attribute determines the first split
            String a = attributeList.get(i);
            
            OrdinalContractor<?> outerContractor = (OrdinalContractor) dataset
                .getContract()
                .getAttributeContract(a);
            
//            System.out.println("First split attribute: " + a);
            
            //System.out.println("Lower bound on left and right size: " + lower);
            for(int j=i+1; j<attributeList.size(); j++)
            {
                String b = attributeList.get(j);
//                System.out.println("\tSecond split attribute: " + b);

                OrdinalContractor<?> innerContractor = (OrdinalContractor) dataset
                    .getContract()
                    .getAttributeContract(b);
                               
                //Sort by values for a and break ties by sorting by b
                List<DataObject> sortedData = dataset
                    .getDataObjects()
                    .stream()
                    .filter(o -> o.get(a) != null)
                    .collect(Collectors.toCollection(ArrayList::new));
                
                Collections.sort(
                    sortedData,
                    DataObject.getProjectionComparator(a, b)
                );
                    
                
                leftToRightIterate(a, b, sortedData, rules, d, outerContractor, innerContractor);
                rightToLeftIterate(a, b, sortedData, rules, d, outerContractor, innerContractor);

            }           
        }
        
        //Add domain rules
        for(String a: attributes)
        {
            rules.add(
                SigmaRuleFactory
                    .createLowerBoundDomainRule(
                        a,
                        (Comparable)dataset
                            .stream()
                            .filter(o -> o.get(a) != null)
                            .map(o -> (Comparable)o.get(a))
                            .min(Comparator.naturalOrder())
                            .get(),
                        (OrdinalContractor) dataset
                            .getContract()
                            .getAttributeContract(a)
                    )
            );
            
            rules.add(
                SigmaRuleFactory
                    .createUpperBoundDomainRule(
                        a,
                        (Comparable)dataset
                            .stream()
                            .filter(o -> o.get(a) != null)
                            .map(o -> (Comparable)o.get(a))
                            .max(Comparator.naturalOrder())
                            .get(),
                        (OrdinalContractor) dataset
                            .getContract()
                            .getAttributeContract(a)
                    )
            );
        }

        
        return new SigmaRuleset(attributes
            .stream()
            .collect(Collectors.toMap(
                a->a,
                a->(OrdinalContractor)dataset.getContract().getAttributeContract(a))),
            rules);
    }

    private void leftToRightIterate(String a, String b, List<DataObject> data, Set<SigmaRule> rules, double d, OrdinalContractor<?> outer, OrdinalContractor<?> inner)
    {
        TreeMap<Comparable, Integer> visited = new TreeMap<>();
                
        int leftCount = 0;
        int rightCount = data.size();

        //For each distinct value in order
        for(Comparable aThreshold: countMap.get(a).keySet())
        {
            int c = countMap.get(a).get(aThreshold);
            leftCount +=    c;
            rightCount -=   c;

            Map<Comparable, Integer> bMap = new HashMap<>();
            
            for(int idx=0;idx<c;idx++)
            {
                //Visit next point
                DataObject o = data.get(leftCount - c + idx);
                if(o.get(b) != null)
                {
                    visited.merge((Comparable)inner.getFromDataObject(o, b), 1, Integer::sum);
                    bMap.merge((Comparable)inner.getFromDataObject(o, b), 1, Integer::sum);
                }
            }
            
            int m = visited.values().stream().mapToInt(Integer::intValue).min().orElse(1);

            //Check lambda sanity
            if(this.lambda < (4.0 * m * d) / Math.pow(d-m, 2.0))
                continue;
            
            //Try to sharpen lower bound
            int lowerBound = (int)Math.ceil((lambda * (d + m) - Math.sqrt( Math.pow(lambda * (d + m), 2.0) - 4.0 * m * lambda * d * (lambda + 1.0))) / (2 * lambda));

            //Apply the first-split pruning strategy
            if(leftCount < lowerBound || rightCount < lowerBound)
                continue;                  

            leftInspect(
                a,
                b,
                aThreshold,
                leftCount,
                c,
                data,
                rules,
                visited,
                bMap,
                d,
                outer,
                inner);

        }
    }
    
    private void rightToLeftIterate(String a, String b, List<DataObject> data, Set<SigmaRule> rules, double d, OrdinalContractor<?> outer, OrdinalContractor<?> inner)
    {
        TreeMap<Comparable, Integer> visited = new TreeMap<>();
        
        int leftCount = data.size();
        int rightCount = 0;

        //For each distinct value in order
        for(Comparable aThreshold: countMap.get(a).descendingKeySet())
        {
            int c = countMap.get(a).get(aThreshold);
            leftCount -=    c;
            rightCount +=   c;

            Map<Comparable, Integer> bMap = new HashMap<>();
            
            for(int idx=0;idx<c;idx++)
            {
                //Visit next point
                DataObject o = data.get(data.size() - rightCount + idx);
                if(o.get(b) != null)
                {
                    visited.merge(inner.getFromDataObject(o, b), 1, Integer::sum);
                    bMap.merge(inner.getFromDataObject(o, b), 1, Integer::sum);
                }
            }
            
            int m = visited
                .values()
                .stream()
                .mapToInt(Integer::intValue)
                .min()
                .orElse(1);
            
            if(this.lambda < (4.0 * m * d) / Math.pow(d-m, 2.0))
                continue;
            
            //Try to sharpen lower bound
            int lowerBound = (int)Math.ceil((lambda * (d + m) - Math.sqrt( Math.pow(lambda * (d + m), 2.0) - 4.0 * m * lambda * d * (lambda + 1.0))) / (2 * lambda));

            //Apply the first-split pruning strategy
            if(leftCount < lowerBound || rightCount < lowerBound)
                continue;                  

            rightInspect(
                a, 
                b,
                aThreshold,
                rightCount,
                c,
                data,
                rules,
                visited,
                bMap,
                d,
                outer,
                inner);
        }
    }
    
    private void leftInspect(String a, String b, Comparable aThreshold, int leftCount, int c, List<DataObject> data, Set<SigmaRule> rules, TreeMap<Comparable, Integer> visited, Map<Comparable, Integer> bMap, double d, OrdinalContractor<?> outer, OrdinalContractor<?> inner)
    {   
        double rightCount = d - leftCount;
        
        //Top part
        for(int idx=0;idx<c;idx++)
        {
            //Next point
           DataObject o = data.get(leftCount - idx - 1);
            
            //Get value for b
            Comparable bValue = (Comparable)inner.getFromDataObject(o, b);
            
            if(bValue == null)
                continue;
            
            Comparable lowerBValue = cumulMap.get(b).lowerKey(bValue);
            
            //Get top left
            double topLeft = visited.tailMap(bValue).values().stream().mapToInt(Integer::intValue).sum();
            
            //Second split pruning
            if(topLeft > (lambda * leftCount * rightCount)/(rightCount*lambda + d))
                break;

            double bottomLeft = leftCount - topLeft;
            
            double topRight = lowerBValue == null ? d : cumulMap.get(b).get(cumulMap.get(b).lastKey()) - cumulMap.get(b).get(lowerBValue) - topLeft;

            if((topLeft * d)/ (bottomLeft * topRight) < lambda)
            {
                Set<AbstractAtom<?,?,?>> atoms = new HashSet<>();
                atoms.add(new ConstantOrdinalAtom<>((OrdinalContractor) outer, a, InequalityOperator.LEQ, aThreshold));
                atoms.add(new ConstantOrdinalAtom<>((OrdinalContractor) inner, b, InequalityOperator.GEQ, bValue));
                
                rules.add(new SigmaRule(atoms));
            }
            
            idx += bMap.get(bValue) - 1;
        }
        
        //Bottom part
        for(int idx=0;idx<c;idx++)
        {
            //Next point
            DataObject o = data.get(leftCount - c + idx);
            
            //Get value for b
            Comparable bValue = inner.getFromDataObject(o, b);
            
            if(bValue == null)
                continue;
            
            //Get bottom left
            double bottomLeft = visited.headMap(bValue, true).values().stream().mapToInt(Integer::intValue).sum();
            
            //Second split pruning
            if(bottomLeft > (lambda * leftCount * rightCount)/(rightCount*lambda + d))
                break;

            double topLeft = leftCount - bottomLeft;
            double bottomRight = cumulMap.get(b).get(bValue) - bottomLeft;

            if((bottomLeft * d) / (topLeft * bottomRight) < lambda)
            {
                Set<AbstractAtom<?,?,?>> atoms = new HashSet<>();
                atoms.add(new ConstantOrdinalAtom<>((OrdinalContractor) outer, a, InequalityOperator.LEQ, aThreshold));
                atoms.add(new ConstantOrdinalAtom<>((OrdinalContractor) inner, b, InequalityOperator.LEQ, bValue));
                
                rules.add(new SigmaRule(atoms));
            }
            
            idx += bMap.get(bValue) - 1;
        }
    }
    
    private void rightInspect(String a, String b, Comparable aThreshold, int rightCount, int c, List<DataObject> data, Set<SigmaRule> rules, TreeMap<Comparable, Integer> visited, Map<Comparable, Integer> bMap, double d, OrdinalContractor<?> outer, OrdinalContractor<?> inner)
    {   
        double leftCount = d - rightCount;
        
        //Top part
        for(int idx=0;idx<c;idx++)
        {
            //Next point
           DataObject o = data.get(data.size() - rightCount + c - idx - 1);
            
            //Get value for b
            Comparable bValue = inner.getFromDataObject(o, b);
            
            if(bValue == null)
                continue;
            
            Comparable lowerBValue = cumulMap.get(b).lowerKey(bValue);
            
            //Get top right
            double topRight = visited.tailMap(bValue).values().stream().mapToInt(Integer::intValue).sum();
            
            //Second split pruning
            if(topRight > (lambda * leftCount * rightCount)/(leftCount*lambda + d))
                break;

            double bottomRight = rightCount - topRight;
            
            double topLeft = lowerBValue == null ? d : cumulMap.get(b).get(cumulMap.get(b).lastKey()) - cumulMap.get(b).get(lowerBValue) - topRight;

            if((topRight * d)/ (bottomRight * topLeft) < lambda)
            {
                Set<AbstractAtom<?,?,?>> atoms = new HashSet<>();
                atoms.add(new ConstantOrdinalAtom<>((OrdinalContractor) outer, a, InequalityOperator.GEQ, aThreshold));
                atoms.add(new ConstantOrdinalAtom<>((OrdinalContractor) inner, b, InequalityOperator.GEQ, bValue));
                
                rules.add(new SigmaRule(atoms));                
            }
            
            idx += bMap.get(bValue) - 1;
        }
        
        //Bottom part
        for(int idx=0;idx<c;idx++)
        {
            //Next point
            DataObject o = data.get(data.size() - rightCount + idx);
            
            //Get value for b
            Comparable bValue = inner.getFromDataObject(o, b);
            
            if(bValue == null)
                continue;
            
            //Get bottom right
            double bottomRight = visited.headMap(bValue, true).values().stream().mapToInt(Integer::intValue).sum();
            
            //Second split pruning
            if(bottomRight > (lambda * leftCount * rightCount)/(leftCount*lambda + d))
                break;

            double topRight = rightCount - bottomRight;
            double bottomLeft = cumulMap.get(b).get(bValue) - bottomRight;

            if((bottomRight * d) / (topRight * bottomLeft) < lambda)
            {
                Set<AbstractAtom<?,?,?>> atoms = new HashSet<>();
                atoms.add(new ConstantOrdinalAtom<>((OrdinalContractor) outer, a, InequalityOperator.GEQ, aThreshold));
                atoms.add(new ConstantOrdinalAtom<>((OrdinalContractor) inner, b, InequalityOperator.LEQ, bValue));
                
                rules.add(new SigmaRule(atoms));                
            }
            
            idx += bMap.get(bValue) - 1;
        }
    }
}
