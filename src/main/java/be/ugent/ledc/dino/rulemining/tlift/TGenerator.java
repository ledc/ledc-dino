package be.ugent.ledc.dino.rulemining.tlift;


import be.ugent.ledc.core.operators.UnitScore;
import be.ugent.ledc.core.operators.aggregation.tnorm.BasicTNorm;
import be.ugent.ledc.core.operators.aggregation.tnorm.TNorm;
import be.ugent.ledc.sigma.datastructures.fptree.AttributeValue;
import be.ugent.ledc.sigma.datastructures.fptree.FPTree;
import be.ugent.ledc.sigma.datastructures.fptree.Itemset;
import java.util.HashMap;

public class TGenerator extends MinGenerator
{
    private TNorm tnorm;
    
    public TGenerator(double tau, boolean ignoreNulls, TNorm tnorm)
    {
        super(tau, ignoreNulls);
        this.tnorm = tnorm;
    }
    
    public TGenerator(double tau, boolean ignoreNulls)
    {
        this(tau, ignoreNulls, BasicTNorm.PRODUCT);
    }

    @Override
    protected double lift(FPTree<AttributeValue<String>> tree, Itemset<AttributeValue<String>> itemset, HashMap<Itemset<AttributeValue<String>>, Integer> supports, int size)
    {
        double numerator = ((double)supports.get(itemset)) / ((double) size);
        
        double minInteraction = Double.MAX_VALUE;

        for (Itemset<AttributeValue<String>> subset : itemset.getSubSets()) {

            if (!subset.isEmpty() && !subset.equals(itemset)) {

                Itemset<AttributeValue<String>> complement = new Itemset<>(itemset);
                complement.removeAll(subset);

                if (supports.get(complement) == null) {
                    supports.put(complement, tree.support(complement));
                }

                if (supports.get(subset) == null) {
                    supports.put(subset, tree.support(subset));
                }

                double complementFrequency  = ((double)supports.get(complement)) / ((double) size);
                double subsetFrequency      = ((double)supports.get(subset)) / ((double) size);

                double interaction = tnorm.aggregate(new UnitScore(complementFrequency), new UnitScore(subsetFrequency)).getValue();

                if (interaction < minInteraction) {
                    minInteraction = interaction;
                }
            }
        }


        if (minInteraction == Integer.MAX_VALUE)
        {
            return 0.0;
        }
        else
        {
            return  numerator / minInteraction;
        }
    }

}
