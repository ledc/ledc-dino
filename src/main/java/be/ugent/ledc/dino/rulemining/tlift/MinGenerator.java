package be.ugent.ledc.dino.rulemining.tlift;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.dino.rulemining.AssociationBasedMiner;
import be.ugent.ledc.core.operators.UnitScore;
import be.ugent.ledc.core.operators.aggregation.tnorm.TNorm;
import be.ugent.ledc.sigma.datastructures.fptree.AttributeValue;
import be.ugent.ledc.sigma.datastructures.fptree.FPTree;
import be.ugent.ledc.sigma.datastructures.fptree.FPTreeFactory;
import be.ugent.ledc.sigma.datastructures.fptree.Itemset;
import be.ugent.ledc.sigma.datastructures.fptree.ItemsetConvertor;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MinGenerator extends AssociationBasedMiner
{
    private final double tau;

    public MinGenerator(double tau, boolean ignoreNulls) {
        super(ignoreNulls);
        this.tau = tau;
    }

    public MinGenerator(double tau) {
        this.tau = tau;
    }

    @Override
    public SigmaRuleset findRules(ContractedDataset dataset, Set<String> attributes)
    {
        if(attributes.isEmpty())
            return new SigmaRuleset(new HashMap<>(), new HashSet<>());
        
        SigmaRuleset domainRuleset = super.findRules(dataset, attributes);
        
        List<Itemset<AttributeValue<String>>> listDataset = ItemsetConvertor
            .convertAsStrings(
                dataset,
                isIgnoreNulls(),
                attributes);
        
        // Update: Build an FP-tree with a minimum support bound equal to 1/tau, since items with a lower support will never
        // generate inconsistent itemsets and they cannot be inconsistent themselves
        FPTree<AttributeValue<String>> tree = FPTreeFactory.makeTree(listDataset, (int) Math.floor(1.0 / tau));

        //Invalidate too frequent items
        tree.generateCandidates((int) Math.ceil(((double) dataset.getSize()) * tau));
        
        //This HashMap object maps an itemset to its support (used to calculate the lift efficiently)
        HashMap<Itemset<AttributeValue<String>>, Integer> supports = new HashMap<>();

        List<Itemset<AttributeValue<String>>> candidates = searchCandidates(tree, new Itemset<>(), supports, tau);

        // Filter out all itemsets that meet the lift constraint
        List<SigmaRule> rules = getSigmaRules(tree, supports, candidates, tau, dataset.getSize());

//        System.out.println(rules.size() + " editrules found.");


        return new SigmaRuleset(
            domainRuleset.getContractors(),
            Stream.concat(
                domainRuleset.stream(),
                rules.stream()
            )
            .collect(Collectors.toSet())
        );

    }

    private List<Itemset<AttributeValue<String>>> searchCandidates(FPTree<AttributeValue<String>> tree, Itemset<AttributeValue<String>> prefix, Map<Itemset<AttributeValue<String>>, Integer> supports, double tau) {
        // Initialize a List of forbidden itemsets
        List<Itemset<AttributeValue<String>>> candidates = new ArrayList<>();

        for (AttributeValue<String> item : tree.getCandidateItems()) {
            Itemset<AttributeValue<String>> newPrefix = prefix.union(new Itemset<>(item));

            // Calculate support of item in current tree and puts this into the supports map
            int support = tree.support(item);
            
            //Store the calculated support for later lift calculations
            supports.put(newPrefix, support);

            // Store the itemset as a candidate if we have currently no evidence that itemset is inconsistent
            // Update: an itemset consisting of one item (so with an empty prefix) is never inconsistent
            if (!prefix.isEmpty() && (double) support / (double) supports.get(prefix) <= tau) {
                candidates.add(newPrefix);
            }

            //Check whether the support of newPrefix is too low for any superset to be inconsistent
            if (support < 1.0 / tau && tau <= 1.) {
                continue;
            }

            // Create the conditional FPT object for given item
            FPTree<AttributeValue<String>> conditionalFPTree = FPTreeFactory.createConditionalFPT(tree, item, 1);

            //Calculate a new upper bound based on the support of the new prefix
            int upperBound = (int) Math.ceil(((double) support) * tau);

            //Generate candidate items
            conditionalFPTree.generateCandidates(upperBound);

            // Recursive call with the conditional FPT object
            if (!conditionalFPTree.getCandidateItems().isEmpty()) {
                List<Itemset<AttributeValue<String>>> subPossibleForbiddenItemSet = searchCandidates(conditionalFPTree, newPrefix, supports, tau);
                candidates.addAll(subPossibleForbiddenItemSet);
            }
        }

        return candidates;
    }

    private List<SigmaRule> getSigmaRules(FPTree<AttributeValue<String>> tree, HashMap<Itemset<AttributeValue<String>>, Integer> supports, List<Itemset<AttributeValue<String>>> candidates, double tau, int size) {

        List<SigmaRule> minedRules = new ArrayList<>();
        
        for (Itemset<AttributeValue<String>> candidate : candidates) {
            double lift = lift(tree, candidate, supports, size);

            if (lift <= tau && candidate.size() > 1) 
            {
                SigmaRule minedRule = convertItemsetToSigmaRule(candidate);
                                
//                String info = Arrays.asList(0.075, 0.05, 0.0, -0.05, -0.075, -1.0)
//                    .stream()
//                    .map(p ->  String.format("%.3f", liftByT(tree, candidate, supports, size, SchweizerSklarFactory.create(p))))
//                    .collect(Collectors.joining(","));
//                
//                System.out.println("\"" + editRule + "\", " + info + ", " + String.format("%.3f", lift));
                
                minedRules.add(minedRule);
            }
        }

        return minedRules;

    }

    protected double lift(FPTree<AttributeValue<String>> tree, Itemset<AttributeValue<String>> itemset, HashMap<Itemset<AttributeValue<String>>, Integer> supports, int size) {

        int numerator = supports.get(itemset);
        
        int minSupport = size;

        for (AttributeValue<String> item : itemset) {
            Itemset<AttributeValue<String>> copy = new Itemset<>(itemset);
            copy.remove(item);

            if (!copy.isEmpty()) {
                int support = supports.get(copy) == null ? tree.support(copy) : supports.get(copy);

                if (support < minSupport) {
                    minSupport = support;
                }
            }
        }

        return ((double) numerator) / ((double) minSupport);
    }
    
    private double liftByT(FPTree<AttributeValue<String>> tree, Itemset<AttributeValue<String>> itemset, HashMap<Itemset<AttributeValue<String>>, Integer> supports, int size, TNorm tnorm)
    {
        double numerator = ((double)supports.get(itemset)) / ((double) size);
        
        double minInteraction = Double.MAX_VALUE;

        for (Itemset<AttributeValue<String>> subset : itemset.getSubSets()) {

            if (!subset.isEmpty() && !subset.equals(itemset)) {

                Itemset<AttributeValue<String>> complement = new Itemset<>(itemset);
                complement.removeAll(subset);

                if (supports.get(complement) == null) {
                    supports.put(complement, tree.support(complement));
                }

                if (supports.get(subset) == null) {
                    supports.put(subset, tree.support(subset));
                }

                double complementFrequency  = ((double)supports.get(complement)) / ((double) size);
                double subsetFrequency      = ((double)supports.get(subset)) / ((double) size);

                double interaction = tnorm.aggregate(new UnitScore(complementFrequency), new UnitScore(subsetFrequency)).getValue();

                if (interaction < minInteraction)
                {
                    minInteraction = interaction;
                }
            }
        }


        if (minInteraction == Integer.MAX_VALUE)
        {
            return 0.0;
        }
        else
        {
            return  numerator / minInteraction;
        }
    }
}
