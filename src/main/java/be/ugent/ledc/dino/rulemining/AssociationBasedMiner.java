package be.ugent.ledc.dino.rulemining;

import be.ugent.ledc.core.DataException;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.dino.RuleMiner;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.fptree.AttributeValue;
import be.ugent.ledc.sigma.datastructures.fptree.Itemset;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantNominalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A Miner that searches for inconsistencies based on association analysis
 * @author abronsel
 */
public abstract class AssociationBasedMiner implements RuleMiner<ContractedDataset>
{
    private final boolean ignoreNulls;

    public AssociationBasedMiner(boolean ignoreNulls) {
        this.ignoreNulls = ignoreNulls;
    }

    public AssociationBasedMiner()
    {
        this(true);
    }

    @Override
    public SigmaRuleset findRules(ContractedDataset dataset, Set<String> attributes)
    {
        for(String a: attributes)
        {
            TypeContractor<?> attributeContract = dataset
                .getContract()
                .getAttributeContract(a);
            
            if(attributeContract == null)
                throw new DataException("Illegal null contract in contracted dataset for attribute " + a);
            
            if(!attributeContract.getClass().isAssignableFrom(TypeContractorFactory.STRING.getClass()))
                throw new DataException("Association based mining operates only on string contracted attributes. Violation found for " + a + ".");

        }
        
        Set<SigmaRule> domainRules = dataset
            .getContract()
            .getAttributes()
            .stream()
            .filter(attributes::contains)
            .map(a -> SigmaRuleFactory.createNominalDomainRule(
                        a,
                        dataset
                            .project(a)
                            .distinct()
                            .stream()
                            .filter(o -> o.get(a) != null)
                            .map(o -> o.getString(a))
                            .collect(Collectors.toSet()),
                         SigmaContractorFactory.STRING)
            )
            .collect(Collectors.toSet());
        
        Map<String, SigmaContractor<?>> contractors = dataset
            .getContract()
            .getAttributes()
            .stream()
            .filter(attributes::contains)
            .collect(Collectors.toMap(
                a -> a,
                a -> SigmaContractorFactory.STRING)
            );
        
        return new SigmaRuleset(contractors, domainRules);
    }
    
    protected SigmaRule convertItemsetToSigmaRule(Itemset<AttributeValue<String>> itemset)
    {
        Set<AbstractAtom<?,?,?>> atoms = new HashSet<>();

        for (AttributeValue<String> item : itemset)
        {
            atoms.add(new ConstantNominalAtom<>(
                SigmaContractorFactory.STRING,
                item.getAttribute(),
                EqualityOperator.EQ,
                item.getValue()
            ));
        }

        return new SigmaRule(atoms);
    }

    public boolean isIgnoreNulls()
    {
        return ignoreNulls;
    }
}
