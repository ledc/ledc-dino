package be.ugent.ledc.dino.rulemining.association;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.dino.rulemining.AssociationBasedMiner;
import be.ugent.ledc.dino.rulemining.FPGrowthAlgorithm;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.fptree.AttributeValue;
import be.ugent.ledc.sigma.datastructures.fptree.FPTree;
import be.ugent.ledc.sigma.datastructures.fptree.FPTreeFactory;
import be.ugent.ledc.sigma.datastructures.fptree.Itemset;
import be.ugent.ledc.sigma.datastructures.fptree.ItemsetConvertor;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import java.util.stream.Stream;

public class ConfidenceMiner extends AssociationBasedMiner
{
    /**
     * The threshold for support in the algorithm
     */
    private final double alpha;

    /**
     * The threshold for confidence in the algorithm
     */
    private final double beta;
    
    /**
     * The threshold for confidence ratio in the algorithm
     */
    private final double sigma;
    
    /**
     * The threshold for error rate
     */
    private final double epsilon;
    
    /**
     * When true, forces exceptions to rules detected via epsilon to be added to the rule.
     * This implies that we no longer deal with association rules.
     */
    private final boolean includeExceptions;
    
    private ConfidenceMiner(ConfidenceMinerBuilder builder)
    {
        super(builder.ignoreNulls);
        this.alpha = builder.alpha;
        this.beta = builder.beta;
        this.sigma = builder.sigma;
        this.epsilon = builder.epsilon;
        this.includeExceptions = builder.includeExceptions;
    }

    /**
     *
     * @param dataset
     * @param attributes
     * @return
     */
    @Override
    public SigmaRuleset findRules(ContractedDataset dataset, Set<String> attributes)
    {               
        if(attributes.isEmpty())
            return new SigmaRuleset(new HashMap<>(), new HashSet<>());
        
        SigmaRuleset domainRuleset = super.findRules(dataset, attributes);
        
        //1. Mine patterns with minimal support
        //System.out.println("Searching frequent itemsets with threshold " + alpha);
        
        List<Itemset<AttributeValue<String>>> convertedData = ItemsetConvertor
            .convertAsStrings(
                dataset,
                isIgnoreNulls(),
                attributes);
        
        Map<Itemset<AttributeValue<String>>, Integer> frequentItemsets = new FPGrowthAlgorithm<AttributeValue<String>>()
                .generateFrequentItemsetsWithCount(convertedData, alpha);

        FPTree<AttributeValue<String>> fullTree = FPTreeFactory.makeTree(convertedData, 1);

        System.out.println("Found " + frequentItemsets.size() + " frequent itemsets");

        //Filter (itemsets need to contain at least two items) and sort the rules (smallest itemset first)
        List<Itemset<AttributeValue<String>>> sorted = frequentItemsets
            .keySet()
            .stream()
            .filter(i -> i.size() >= 2)
            .sorted((i1, i2) -> Integer.valueOf(i1.size()).compareTo(i2.size()))
            .collect(Collectors.toList());

        //Keep a map of RHS to LHS to detect redundancy and impossible strong rules
        Map<AttributeValue<String>, Set<Itemset<AttributeValue<String>>>> cache = new HashMap<>();
        
        List<AttributeValue<String>> singletons = frequentItemsets
            .keySet()
            .stream()
            .filter(i -> i.size() == 1)
            .flatMap(i -> i.stream())
            .collect(Collectors.toList());        
        
        //Populate the cache
        for (AttributeValue<String> rhs : singletons)
        {
            cache.put(rhs, new HashSet<>());
        }
        
        //2. Get rules with VERY HIGH confidence
        //System.out.println("Searching association rules with threshold " + beta);
        Set<SigmaRule> sigmaRules = new HashSet<>();
        
        Map<AttributeValue<String>, Set<AttributeValue<String>>> confidenceCache = new HashMap<>();
        
        for(AttributeValue<String> rhs: singletons)
        {
            confidenceCache.put(rhs, new HashSet<>());
        }
        
        double dSize = (double) dataset.getSize();
        
        for (int j=0; j<sorted.size();j++)
        {
            Itemset<AttributeValue<String>> generator = sorted.get(j);
                        
            int ruleCount = frequentItemsets.get(generator);
            
            double ruleSupport = ((double) ruleCount) / dSize;
            
            for (AttributeValue<String> rhs : generator)
            {
                if(generator.size() > 2 && confidenceCache.get(rhs).stream().anyMatch(a -> generator.contains(a)))
                    continue;
                
                Itemset<AttributeValue<String>> lhs = new Itemset<>(generator);
                lhs.remove(rhs);
                                
                int lhsCount = frequentItemsets.get(lhs);
                int rhsCount = frequentItemsets.get(new Itemset<>(rhs));
                
                double lhsSupport = ((double)lhsCount) / dSize;
                double rhsSupport = ((double)rhsCount) / dSize;

                double confidence = ruleSupport / lhsSupport;
                
                //If the LHS contains one attribute and the rule has a high confidence, we remember that rules containing this LHS and have the rhs can not be strong
                if(lhs.size() == 1 && confidence * sigma > 1.0)
                    confidenceCache.get(rhs).addAll(lhs.stream().collect(Collectors.toSet()));                
                
                if(confidence < beta)
                    continue;
                
                //Check redundancy w.r.t. already established rules and verify whether confidence is above the desired threshold
                if (cache.get(rhs).stream().anyMatch(condition -> lhs.containsAll(condition)))
                    continue;
                
                //Verify whether the rule is strong w.r.t. the given sigma
                if(!strongRuleVerification(lhs, rhs, frequentItemsets, confidence, rhsSupport))
                    continue;
                
                
                Set<AttributeValue<String>> exceptions = errorRateVerification(
                    dataset,
                    lhs, 
                    rhs,
                    fullTree);
                
                if(!includeExceptions && !exceptions.isEmpty())
                    continue;
                
//              System.out.println("Found rule IF " + lhs + " THEN " + rhs);
//              System.out.println("Found rule IF " + lhs + " THEN " + rhs + " with confidence " + (ruleSupport/lhsSupport));
    
                Set<AbstractAtom<?,?,?>> atoms = lhs
                    .stream()
                    .map(av -> new ConstantNominalAtom<>
                        (
                            SigmaContractorFactory.STRING,
                            av.getAttribute(),
                            EqualityOperator.EQ,
                            av.getValue())
                    )
                    .collect(Collectors.toSet());

                if(exceptions.isEmpty())
                {
                    cache.get(rhs).add(lhs);
                    
                    atoms.add(new ConstantNominalAtom(
                        SigmaContractorFactory.STRING,
                        rhs.getAttribute(),
                        EqualityOperator.NEQ,
                        rhs.getValue()));
                }
                else
                {
                    //No caching...
                    
                    atoms.add(
                        new SetNominalAtom<>
                        (
                            SigmaContractorFactory.STRING,
                            rhs.getAttribute(),
                            SetOperator.NOTIN,
                            Stream.concat(
                                Stream.of(rhs.getValue()),
                                exceptions.stream().map(ex -> ex.getValue())
                            ).collect(Collectors.toSet())
                        )
                    );
                }
                
                SigmaRule minedRule = new SigmaRule(atoms);
                sigmaRules.add(minedRule);
                
            }
        }

        return new SigmaRuleset(
            domainRuleset.getContractors(),
            Stream.concat(
                domainRuleset.stream(),
                sigmaRules.stream()
            )
            .collect(Collectors.toSet())
        );
    }
    
    private Set<AttributeValue<String>> errorRateVerification(ContractedDataset dataset, Itemset<AttributeValue<String>> lhs, AttributeValue<String> rhs, FPTree<AttributeValue<String>> fullTree)
    {
        Set<AttributeValue<String>> alternatives =
            dataset
            .getDataObjects()
            .stream()
            .filter(o -> o.get(rhs.getAttribute()) != null)
            .map(o -> o.getString(rhs.getAttribute()))
            .distinct()
            .filter(value -> !value.equals(rhs.getValue()))
            .map(value -> new AttributeValue<>(rhs.getAttribute(), value))
            .collect(Collectors.toSet());
                       
        Set<AttributeValue<String>> expectionsToTheRule = new HashSet<>();
        
        for(AttributeValue<String> alternative: alternatives)
        {
            Itemset<AttributeValue<String>> errorPattern = new Itemset<>(lhs);
            errorPattern.add(alternative);

            int alternativeRhsSupport = fullTree.support(alternative);
            int errorPatternSupport = fullTree.support(errorPattern);

            double valueErrorRate = ((double)errorPatternSupport)/((double)alternativeRhsSupport);
            
            if(valueErrorRate > this.epsilon)
            {
//                System.out.println("Discarding rule IF " + lhs + " THEN " + rhs + " because of value " + alternative.getValue() +  " with error rate " + String.format("%.4f", valueErrorRate));
                expectionsToTheRule.add(alternative);
            }            
        }
        
        return expectionsToTheRule;
    }
    
    private boolean strongRuleVerification(Itemset<AttributeValue<String>> lhs, AttributeValue<String> rhs, Map<Itemset<AttributeValue<String>>, Integer> frequentItemsets, double confidence, double rhsSupport)
    {
        //At this point, we are sure that confidence exceeds confidence of stronger rules. Now verify wether each condition in LHS has sufficient strength
        if(lhs.size() == 1)
        {
            //If there is only one condition, we should compare to a rule that always predicts the RHS (This rule has a confidence equal to the support of the RHS
            if(confidence/rhsSupport < sigma)
            {
                return false;
            }
        }
        else
        {
            for(AttributeValue<String> lhsItem: lhs)
            {
                //Consider a LHS condition without the lhsItem
                Itemset<AttributeValue<String>> subLhs = new Itemset<>(lhs);
                subLhs.remove(lhsItem);

                Itemset<AttributeValue<String>> subGenerator = new Itemset<>(lhs);
                subGenerator.remove(lhsItem);
                subGenerator.add(rhs);

                //Calculate confidence for rule lhsSubset => rhs (we are sure that it is below beta)
                int lhsSubsetCount = frequentItemsets.get(subLhs);
                int subRuleCount = frequentItemsets.get(subGenerator);

                double subConfidence = ((double) subRuleCount) / ((double) lhsSubsetCount);

                if(confidence / subConfidence < sigma)
                {
                    return false;
                }
            }
        }
        
        return true;
    }

    public double getAlpha()
    {
        return alpha;
    }

    public double getBeta()
    {
        return beta;
    }

    public double getSigma()
    {
        return sigma;
    }

    public double getEpsilon()
    {
        return epsilon;
    }
    
    public static class ConfidenceMinerBuilder
    {
        private final double alpha;
        private final double beta;
        private double sigma;
        private double epsilon;
        private boolean ignoreNulls;
        private boolean includeExceptions;
        
        public ConfidenceMinerBuilder(double alpha, double beta)
        {
            this.alpha = alpha;
            this.beta = beta;
            this.sigma = 1.0;
            this.epsilon = 1.0;
            this.ignoreNulls = true;
            this.includeExceptions = false;
        }
        
        public ConfidenceMinerBuilder withSigma(double sigma)
        {
            this.sigma = sigma;
            return this;
        }
        
        public ConfidenceMinerBuilder withEpsilon(double epsilon)
        {
            this.epsilon = epsilon;
            return this;
        }
        
        public ConfidenceMinerBuilder withIgnoreNulls(boolean ignoreNulls)
        {
            this.ignoreNulls = ignoreNulls;
            return this;
        }
        
        public ConfidenceMinerBuilder withIncludeExceptions(boolean includeExceptions)
        {
            this.includeExceptions = includeExceptions;
            return this;
        }
        
        public ConfidenceMiner build()
        {
            return new ConfidenceMiner(this);
        }
    }
}
