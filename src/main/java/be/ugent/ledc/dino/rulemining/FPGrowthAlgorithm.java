package be.ugent.ledc.dino.rulemining;

import be.ugent.ledc.sigma.datastructures.fptree.FPTree;
import be.ugent.ledc.sigma.datastructures.fptree.FPTreeFactory;
import be.ugent.ledc.sigma.datastructures.fptree.Itemset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FPGrowthAlgorithm<I>
{
    public static boolean PRINT_TREE = false;

    public Map<Itemset<I>, Integer> generateFrequentItemsetsWithCount(List<Itemset<I>> dataset, double supportLowerBound) {

        int minimumSupportCount = (int) Math.ceil(supportLowerBound * (double) dataset.size());
             
        // Create an FPT object from the given Dataset object and the relative support lower bound
        FPTree<I> fpTree = FPTreeFactory.makeTree(dataset, minimumSupportCount);
        
        if (PRINT_TREE) {
            fpTree.print();
        }

        // Run FPGrowth algorithm on this FPTree object with given minimum support count
        Map<Itemset<I>, Integer> frequentItemsets = FPGrowth(fpTree, new Itemset<>(), minimumSupportCount);

        return frequentItemsets;
    }

    private Map<Itemset<I>, Integer> FPGrowth(FPTree<I> fpTree, Itemset<I> suffix, int minimumSupportCount) {

        // Initialize a list of frequent itemsets
        Map<Itemset<I>, Integer> frequentItemSets = new HashMap<>();

        // Get all items appearing in the FPTree object
        Itemset<I> items = new Itemset<>(fpTree.getItemOrder());

        // If the FPTree object only has one path
        if (fpTree.isSinglePath())
        {
            // Add all combinations of items in the ItemSet
            for (Itemset<I> subset : items.getSubSets())
            {
                if (!subset.isEmpty())
                {
                    int support = fpTree.support(subset);
                    Itemset<I> itemset = new Itemset<>(subset);
                    itemset.addAll(suffix);
                    frequentItemSets.put(itemset, support);
                }
            }
        }
        else
        {
            for (I item : items)
            {
                // Add item to the current suffix
                Itemset<I> currentSet = suffix.union(new Itemset<>(item));
                
                // Add current Itemset object to the List of frequent itemsets
                frequentItemSets.put(currentSet, fpTree.support(item));

                // Create the conditional FPT object for given item
                FPTree<I> conditionalFPTree = FPTreeFactory.createConditionalFPT(fpTree, item, minimumSupportCount);

                // Recursive call with the conditional FPT object
                if (!conditionalFPTree.isEmpty())
                {
                    Map<Itemset<I>, Integer> subFrequentItemSet = FPGrowth(conditionalFPTree, currentSet, minimumSupportCount);
                    frequentItemSets.putAll(subFrequentItemSet);
                }
            }
        }
        return frequentItemSets;
    }
}
