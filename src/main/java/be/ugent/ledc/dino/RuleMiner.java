package be.ugent.ledc.dino;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.Set;

public interface RuleMiner<D extends Dataset>
{
    default SigmaRuleset findRules(D dataset, String ... attributes)
    {
        return findRules(dataset, SetOperations.set(attributes));
    }
    
    SigmaRuleset findRules(D dataset, Set<String> attributes);
}