package be.ugent.ledc.dino;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.util.SetOperations;
import java.util.Map;
import java.util.Set;

public interface OutlierDetector<D extends Dataset, S extends Comparable<? super S>>
{   
    default Map<DataObject, S> findOutliers(D dataset, String ... attributes)
    {
        return findOutliers(dataset, SetOperations.set(attributes));
    }
    
    Map<DataObject, S> findOutliers(D dataset, Set<String> attributes);
}
