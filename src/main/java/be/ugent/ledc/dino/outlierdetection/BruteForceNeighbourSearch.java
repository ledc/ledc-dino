package be.ugent.ledc.dino.outlierdetection;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.kdtree.NearestNeighbours;
import be.ugent.ledc.core.operators.metric.Metric;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BruteForceNeighbourSearch extends AbstractNeighbourSearch
{
    private final Dataset dataset;

    public BruteForceNeighbourSearch(Set<String> attributes, Dataset dataset)
    {
        super(attributes);
        this.dataset = dataset;
    }
        
    @Override
    public List<DataObject> getNeighbourhood(DataObject point, double radius, Metric<Double, DataObject> metric)
    {
        return dataset
            .getDataObjects()
            .stream()
            .filter(n -> point != n && metric
                .distance(
                    n.project(getAttributes()),
                    point.project(getAttributes())) <= radius)
            .collect(Collectors.toList());

    }

    @Override
    public List<DataObject> getKNearestNeighbours(DataObject point, int k, Metric<Double, DataObject> metric)
    {
        NearestNeighbours nn = new NearestNeighbours(k);
        
        DataObject q = point.project(getAttributes());
        
        for(DataObject o: dataset)
        {
            if(!o.equals(point))
                nn.update(
                    o,
                    metric.distance(q, o.project(getAttributes())),
                    1);
        }
        
        return nn.getNeighbours();
    }
}
