package be.ugent.ledc.dino.outlierdetection.isolationforest;

import be.ugent.ledc.dino.outlierdetection.isolationforest.datastructures.IsolationForestGenerator;
import be.ugent.ledc.dino.outlierdetection.isolationforest.datastructures.IsolationForest;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.dino.OutlierDetector;
import be.ugent.ledc.dino.outlierdetection.isolationforest.splitter.AbstractSplitter;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class IsolationForestDetector<T extends Comparable<T>> implements OutlierDetector<ContractedDataset, Double>
{
    private final AbstractSplitter<T> splitter;
    
    private final double outlierThreshold;
    
    private final int numberOfTrees;
    
    private final int sampleSize;
    
    public IsolationForestDetector(AbstractSplitter<T> splitter, double outlierThreshold, int numberOfTrees, int sampleSize)
    {
        this.splitter = splitter;
        this.outlierThreshold = outlierThreshold;
        this.numberOfTrees = numberOfTrees;
        this.sampleSize = sampleSize;
    }
    
    public IsolationForestDetector(AbstractSplitter<T> splitter, double outlierThreshold)
    {
        this(splitter, outlierThreshold, 100, 256);
    }
    
    @Override
    public Map<DataObject, Double> findOutliers(ContractedDataset dataset, Set<String> attributes)
    {
        IsolationForest<T> forest = new IsolationForestGenerator<>(
            splitter,
            numberOfTrees,
            sampleSize
        )
        .generate(dataset, attributes);
                    
        return dataset
            .stream()
            .filter(o -> forest.anomalyScore(o) > outlierThreshold)
            .distinct()
            .collect(Collectors.toMap(
                o -> o,
                o -> forest.anomalyScore(o)
            ));
    }
    
}
