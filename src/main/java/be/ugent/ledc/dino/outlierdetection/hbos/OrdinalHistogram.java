package be.ugent.ledc.dino.outlierdetection.hbos;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.datastructures.histogram.Histogram;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.util.Map;

public class OrdinalHistogram<D extends Comparable<? super D>> extends Histogram<D>
{
    private final OrdinalContractor<D> contractor;

    public OrdinalHistogram(OrdinalContractor<D> contractor, Map<Interval<D>, Long> binningMap)
    {
        super(binningMap);
        this.contractor = contractor;
    }

    @Override
    public Double binHeight(Interval<D> bin)
    {       
        if(bin == null)
            return null;
        
        return countFor(bin).doubleValue() /
               ((double)contractor.cardinality(bin));
        
    }
    
}
