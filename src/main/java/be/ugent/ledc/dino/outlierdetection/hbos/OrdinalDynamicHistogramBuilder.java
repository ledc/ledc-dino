package be.ugent.ledc.dino.outlierdetection.hbos;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.datastructures.histogram.DynamicHistogramBuilder;
import be.ugent.ledc.core.datastructures.histogram.Histogram;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.util.TreeMap;

public class OrdinalDynamicHistogramBuilder<D extends Comparable<? super D>> extends DynamicHistogramBuilder<D, OrdinalContractor<D>>
{
    @Override
    protected Histogram<D> createHistogram(TreeMap<Interval<D>, Long> binningMap, OrdinalContractor<D> contractor)
    {
        return new OrdinalHistogram<>(contractor, binningMap);
    }
}
