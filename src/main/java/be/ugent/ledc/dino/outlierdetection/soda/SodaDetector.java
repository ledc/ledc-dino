package be.ugent.ledc.dino.outlierdetection.soda;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import be.ugent.ledc.dino.OutlierDetector;
import be.ugent.ledc.dino.outlierdetection.NeighbourSearch;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Implementation of the Singular Outliers detection algorithm.
 * 
 * Algorithm published in "Singular Outliers: Finding Common Observations with an Uncommon Feature"
 * by Mark Pijnenburg and Wojtek Kowalczyk on the IPMU 2018 conference.
 * 
 * Key idea is to search for objects that differ from common objects in only a small
 * amount of features. This is done by computing the local Euclidean-Manhattan ratio (LEMR).
 */
public class SodaDetector implements OutlierDetector<Dataset, Double>
{
    /**
     * The number of nearest neighbors to account for in the LEMR computation
     */
    private final int k;
    
    private final double tau;
    
    private final NeighbourSearch neighbourSearch;

    public SodaDetector(NeighbourSearch neighbourSearch, int k, double tau)
    {
        this.k = k;
        this.tau = tau;
        this.neighbourSearch = neighbourSearch;
    }

    
    @Override
    public Map<DataObject, Double> findOutliers(Dataset dataset, Set<String> attributes)
    {
        //Initialize the outliers
        Map<DataObject, Double> outliers = new HashMap<>();
    
        //For each observation
        for(DataObject observation: dataset)
        {
            //Retrieve the nearest neighbours
            List<DataObject> nearestNeighbours = neighbourSearch
                .getKNearestNeighbours(
                    observation,
                    k,
                    DataObjectMetrics.FAST_MANHATTAN
                );

            //Calculate the trimmed mean
            DataObject trimmedMean = getTrimmedMean(nearestNeighbours, attributes)
                .project(attributes);

            double lemr =     
                DataObjectMetrics.FAST_EUCLIDEAN.distance(observation.project(attributes), trimmedMean)/
                DataObjectMetrics.FAST_MANHATTAN.distance(observation.project(attributes), trimmedMean);
        
            if(lemr >= tau)
            {
                outliers.put(observation, lemr);
            }
        }

        return outliers;

    }
    
    private DataObject getTrimmedMean(List<DataObject> nearestNeighbours, Set<String> attributes)
    {
        DataObject trimmedMean = new DataObject();
        
        for(String a: attributes)
        {
            List<Double> sortedFeatureValues = nearestNeighbours
                .stream()
                .filter(o -> o.get(a) != null)
                .map(o -> o.asDouble(a).getDouble(a))
                .sorted()
                .collect(Collectors.toList());
            
            if(!sortedFeatureValues.isEmpty())
            {
                //Trim first and last value
                sortedFeatureValues.remove(0);
                sortedFeatureValues.remove(sortedFeatureValues.size()-1);
            }
            
            trimmedMean.set(
                a,
                sortedFeatureValues.isEmpty()
                    ? null
                    : sortedFeatureValues
                        .stream()
                        .mapToDouble(d->d)
                        .average()
                        .getAsDouble()
            );
        }
        
        return trimmedMean;
    }
}
