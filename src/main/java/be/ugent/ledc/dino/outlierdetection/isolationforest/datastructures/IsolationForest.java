package be.ugent.ledc.dino.outlierdetection.isolationforest.datastructures;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Interval;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class IsolationForest<T extends Comparable<T>>
{
    private final List<DecisionTree<T>> decisionTrees;
    
    private final int subSampleSize;

    protected IsolationForest(List<DecisionTree<T>> decisionTrees, int subSampleSize)
    {
        this.decisionTrees = decisionTrees;
        this.subSampleSize = subSampleSize;
    }
    
    protected IsolationForest(int subSampleSize)
    {
        this(new ArrayList<>(), subSampleSize);
    }

    public List<DecisionTree<T>> getDecisionTrees()
    {
        return decisionTrees;
    }
    
    public void addDecisionTree(DecisionTree<T> decisionTree)
    {
        this.decisionTrees.add(decisionTree);
    }
    
    public double anomalyScore(DataObject o)
    {
        double averagePathLength = decisionTrees
            .stream()
            .mapToDouble(tree -> tree.calculateAdjustedDepth(o))
            .average()
            .getAsDouble();

        return Math.pow(2.0, -averagePathLength / meanTreeDepth(this.subSampleSize));
    }
    
    public List<Map<String, Interval<T>>> summarizePaths(DataObject o)
    {
        List<Map<String, Interval<T>>> paths = new ArrayList<>();
        
        for(DecisionTree<T> d: decisionTrees)
        {
            Map<String, Interval<T>> path = d.showPath(o);
            if(!path.isEmpty())
                paths.add(path);
        }
        
        Map<String, Interval<T>> shortestPath = paths.stream().sorted(Comparator.comparing(path -> path.size())).findFirst().get();
        
        paths.clear();
        paths.add(shortestPath);
        
        return paths;
    }
    
    public static double meanTreeDepth(int samples)
    {
        if(samples == 2)
            return 1.0;
        if(samples < 2)
            return 0.0;
        
        double s = (double)(samples-1);
        return 2.0 * harmonicNumber(s) - (2.0 * s/(s + 1.0));
    }
    
    private static double harmonicNumber(double x)
    {
        return Math.log(x) + 0.5772156649;
    }
}
