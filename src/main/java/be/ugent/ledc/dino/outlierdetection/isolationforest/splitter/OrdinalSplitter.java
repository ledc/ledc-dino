package be.ugent.ledc.dino.outlierdetection.isolationforest.splitter;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.util.ItemSelector;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OrdinalSplitter<T extends Comparable<T>> extends AbstractSplitter<T>
{
    private final double weightPower;

    public OrdinalSplitter(TypeContractor<T> contractor, double weightPower)
    {
        super(contractor);
        this.weightPower = weightPower;
    }
    
    public OrdinalSplitter(TypeContractor<T> contractor)
    {
        this(contractor, 0.0);
    }
    
    @Override
    public T selectRandomSplit(ContractedDataset sample, String splitAttribute)
    {
        //Count items
        Map<T, Long> countMap = sample
            .getDataObjects()
            .stream()
            .map(o -> getContractor().getFromDataObject(o, splitAttribute))
            .filter(v -> v != null)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        
        //Compute weights
        Map<T, Double> weightMap =
            countMap
            .entrySet()
            .stream()
            .collect(Collectors.toMap(e -> e.getKey(), e -> Math.pow(e.getValue(), weightPower)));
        
        //Compute sum of weights
        double total = weightMap.values().stream().mapToDouble(d->d).sum();
        
        return ItemSelector.selectItemByProbability(weightMap.entrySet().stream().collect(Collectors.toMap(e->e.getKey(), e->e.getValue()/total)));
    }
    
}
