package be.ugent.ledc.dino.outlierdetection.knn;

import java.util.function.Function;
import java.util.stream.DoubleStream;

public enum KNNProcessor implements Function<DoubleStream, Double>
{
    MAX(str -> str.max().getAsDouble()),
    AVG(str -> str.average().getAsDouble());
    
    private final Function<DoubleStream, Double> processor;
    
    private KNNProcessor(Function<DoubleStream, Double> processor)
    {
        this.processor = processor;
    }

    @Override
    public Double apply(DoubleStream str)
    {
        return processor.apply(str);
    }
}
