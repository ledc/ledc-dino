package be.ugent.ledc.dino.outlierdetection.isolationforest.splitter;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import java.util.Random;

public class DoubleSplitter extends AbstractSplitter<Double>
{
    private final Random generator = new Random(System.nanoTime());
    
    public DoubleSplitter()
    {
        super(TypeContractorFactory.DOUBLE);
    }
    
    @Override
    public Double selectRandomSplit(ContractedDataset sample, String splitAttribute)
    {
        Double min = sample
            .getDataObjects()
            .stream()
            .mapToDouble(o -> getContractor().getFromDataObject(o, splitAttribute))
            .min()
            .getAsDouble();
        
        Double max = sample
            .getDataObjects()
            .stream()
            .mapToDouble(o -> getContractor().getFromDataObject(o, splitAttribute))
            .max()
            .getAsDouble();
        
        return min + generator.nextDouble() * (max - min);
    }
}
