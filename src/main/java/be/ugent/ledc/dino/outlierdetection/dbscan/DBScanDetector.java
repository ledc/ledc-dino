package be.ugent.ledc.dino.outlierdetection.dbscan;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import be.ugent.ledc.core.operators.metric.Metric;
import be.ugent.ledc.dino.OutlierDetector;
import be.ugent.ledc.dino.outlierdetection.NeighbourSearch;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * An Outlier detector based on the DBScanDetector algorithm, where clusters are considered
 as dense and connected sets of points in a metric space.
 * @author abronsel
 */
public class DBScanDetector implements OutlierDetector<Dataset, Boolean>
{
    private final double eps;

    private final int minPts;

     private final Metric<Double, DataObject> metric;
    
    private final NeighbourSearch neighbourSearch;
    
    private enum Status
    {
        NOISE,
        PART_OF_CLUSTER
    }

    public DBScanDetector(NeighbourSearch neighbourSearch, double eps, int minPts, Metric<Double, DataObject> metric)
    {
        this.eps = eps;
        this.minPts = minPts;
        this.neighbourSearch = neighbourSearch;
        this.metric = metric;
    }
    
    public DBScanDetector(NeighbourSearch neighbourSearch, double eps, int minPts)
    {
        this(neighbourSearch, eps, minPts, DataObjectMetrics.EUCLIDEAN);
    }

    @Override
    public Map<DataObject, Boolean> findOutliers(Dataset dataset, Set<String> attributes)
    {
        List<Set<DataObject>> clusters = new ArrayList<>();
        
        Map<DataObject, Status> visited = new HashMap<>();
        
        for (DataObject point : dataset.getDataObjects())
        {
            if (visited.get(point) != null)
            {
                continue;
            }
            
            Dataset neighbors = new SimpleDataset(neighbourSearch
                .getNeighbourhood(point, eps, metric));
            
            if (neighbors.getSize() >= minPts)
            {
                //DBSCAN does not care about center points
                Set<DataObject> cluster = new HashSet<>();
                clusters.add(expandCluster(
                    cluster,
                    point,
                    neighbors,
                    dataset,
                    visited,
                    attributes
                ));
            }
            else
            {
                visited.put(point, Status.NOISE);
            }
        }
        
        for(DataObject p: visited.keySet())
        {
            if(visited.get(p) == Status.NOISE)
            {
                Set<DataObject> noiseCluster = new HashSet<>();
                noiseCluster.add(p);
                clusters.add(noiseCluster);
            }
        }
        
//        clusters.forEach(System.out::println);
        
        return visited
            .entrySet()
            .stream()
            .filter(e -> e.getValue() == Status.NOISE)
            .map(e -> e.getKey())
            .collect(Collectors.toMap(
                o -> o,
                o -> Boolean.TRUE
            ));
    }

    private Set<DataObject> expandCluster(Set<DataObject> cluster, DataObject point, Dataset neighbors, Dataset points, Map<DataObject, Status> visited, Set<String> attributes)
    {
        cluster.add(point);
        visited.put(point, Status.PART_OF_CLUSTER);

        Dataset seeds = new SimpleDataset();
        seeds = seeds.unionAll(neighbors);
        
        int index = 0;
        
        while (index < seeds.getSize())
        {
            final DataObject current = seeds.getDataObjects().get(index);
            Status pStatus = visited.get(current);
            
            if (pStatus == null)
            {
                Dataset currentNeighbors = new SimpleDataset(neighbourSearch
                    .getNeighbourhood(point, eps, metric));
                
                if (currentNeighbors.getSize() >= minPts)
                    seeds = merge(seeds, currentNeighbors);
            }

            if (pStatus != Status.PART_OF_CLUSTER)
            {
                visited.put(current, Status.PART_OF_CLUSTER);
                cluster.add(current);
            }

            index++;
        }
        return cluster;
    }

    private Dataset merge(Dataset one, Dataset two)
    {
        Set<DataObject> oneSet = new HashSet<>(one.getDataObjects());
        
        for (DataObject item : two.getDataObjects())
        {
            if (!oneSet.contains(item))
            {
                one.addDataObject(item);
            }
        }
        return one;
    }

}
