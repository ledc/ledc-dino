package be.ugent.ledc.dino.outlierdetection;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.datastructures.kdtree.KDTree;
import be.ugent.ledc.core.datastructures.kdtree.KDTreeFactory;
import be.ugent.ledc.core.operators.metric.Metric;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Nearest Neighbour search that utilizes and kdTree for searching
 * @author abronsel
 * @param <N>
 */
public class TreeNeighbourSearch<N  extends Number & Comparable<? super N>> extends AbstractNeighbourSearch
{
    private final KDTree<N> kdTree;

    public TreeNeighbourSearch(Set<String> attributes, ContractedDataset dataset, TypeContractor<N> contractor)
    {
        super(attributes);
        kdTree = KDTreeFactory.create(dataset, contractor);
    }

    @Override
    public List<DataObject> getNeighbourhood(DataObject point, double radius, Metric<Double, DataObject> metric)
    {
        return kdTree
            .radialSearch(point, radius, metric)
            .stream()
            .collect(Collectors.toList());
    }

    @Override
    public List<DataObject> getKNearestNeighbours(DataObject point, int k, Metric<Double, DataObject> metric)
    {
        return kdTree.kNearest(point, metric, k+1);
    }
}
