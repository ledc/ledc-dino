package be.ugent.ledc.dino.outlierdetection;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.operators.metric.Metric;
import java.util.List;

public interface NeighbourSearch
{
    /**
     * Returns the set of points that lie in a hyper sphere around the given point and with a given radius.
     * Only points that are different from the center point on at least one attribute must be included.
     * @param point
     * @param radius
     * @param metric
     * @return 
     */
    List<DataObject> getNeighbourhood(DataObject point, double radius, Metric<Double, DataObject> metric);
    
    /**
     * Returns a list of k points that:
     * (i) are different from the query point on at least one attribute and
     * (ii) lie closest to the query point, where distance is calculated by a given metric.
     * 
     * Important: the metric might be applied to projections of objects (e.g., those attributes
     * that contain real-values), while different in condition (i) means different on at least
     * one of the attributes.
     * @param query
     * @param metric
     * @param k
     * @return 
     */
    List<DataObject> getKNearestNeighbours(DataObject query, int k, Metric<Double, DataObject> metric);
}
