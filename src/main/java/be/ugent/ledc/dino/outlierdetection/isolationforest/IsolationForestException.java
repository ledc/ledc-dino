package be.ugent.ledc.dino.outlierdetection.isolationforest;

public class IsolationForestException extends Exception
{
    public IsolationForestException(String message)
    {
        super(message);
    }

    public IsolationForestException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public IsolationForestException(Throwable cause)
    {
        super(cause);
    }

    public IsolationForestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
