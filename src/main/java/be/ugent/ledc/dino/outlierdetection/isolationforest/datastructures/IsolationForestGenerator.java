package be.ugent.ledc.dino.outlierdetection.isolationforest.datastructures;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.Contract.ContractBuilder;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.dino.outlierdetection.isolationforest.splitter.AbstractSplitter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class IsolationForestGenerator<T extends Comparable<T>>
{
    private final int numberOfTrees;
    
    private final int sampleSize;
    
    private final AbstractSplitter<T> splitter;
    
    private final Random generator;
 
    public IsolationForestGenerator(AbstractSplitter<T> splitter, int numberOfTrees, int sampleSize)
    {
        this.splitter = splitter;
        this.numberOfTrees = numberOfTrees;
        this.sampleSize = sampleSize;
        this.generator = new Random(System.nanoTime());
    }
    
    public IsolationForestGenerator(AbstractSplitter<T> splitter)
    {
        this(splitter, 100, 256);
    }
    
    public IsolationForest<T> generate(ContractedDataset dataset, Set<String> attributes)
    {
        //Init an empty random forest
        IsolationForest<T> randomForest = new IsolationForest<>(sampleSize);

        //Calculate the maximum tree height
        int maxTreeHeight = (int) Math.ceil(Math.log(sampleSize) / Math.log(2.0));
        
        for(int i=0; i<numberOfTrees; i++)
        {
            randomForest
                .addDecisionTree(
                    growRandomTree(
                        drawSample(dataset),
                        attributes,
                        maxTreeHeight
                    )
                );
        }
        
        return randomForest;
    }
    
    private ContractedDataset drawSample(ContractedDataset dataset)
    {        
        //Init a set of indices to select samples
        Set<Integer> indices = new HashSet<>();
        
        //Compose a set of uniques indices
        while(indices.size() < sampleSize)
        {
            indices.add(generator.nextInt(dataset.getSize()));
        }
        
        //Map indices to objects from the dataset and return the sample
        List<DataObject> selection = indices
            .stream()
            .map(index -> dataset
                .getDataObjects()
                .get(index))
            .collect(Collectors.toList());
        
        return wrap(dataset.getContract(), selection, dataset.getContract().getAttributes());
    }
    
    private DecisionTree<T> growRandomTree(ContractedDataset sample, Set<String> attributes, int maxTreeHeight)
    {
        DecisionNode<T> root = split(sample, new ArrayList<>(attributes), maxTreeHeight);
        
        return new DecisionTree<>(
            root,
            maxTreeHeight,
            splitter.getContractor()
        );
    }
    
    private DecisionNode<T> split(ContractedDataset sample, List<String> attributes, int splitsLeft)
    {
        if(sample.getSize() <= 1 || splitsLeft == 0)
            return new DecisionNode<>(null, null, sample.getSize());
        
        List<String> newAttributes = new ArrayList(attributes);
        
        //Check attributes for sanity
        for(String attribute: attributes)
        {
            if(sample
                .getDataObjects()
                .stream()
                .map(o -> o
                    .get(attribute))
                    .allMatch(v -> v == null)
            )
            {
                newAttributes.remove(attribute);
            }
        }
        
        String splitAttribute = selectSplitAttribute(newAttributes);
        
        //Select a threshold
        T threshold = splitter.selectRandomSplit(sample, splitAttribute);
        
        //To perform the split, first the objects with missing values
        List<DataObject> missing = sample
            .getDataObjects()
            .stream()
            .filter(o -> o.get(splitAttribute) == null)
            .collect(Collectors.toList());
        
        DecisionNode<T> node = new DecisionNode<>(splitAttribute, threshold, sample.getSize());
        
        //Then split left and right
        List<DataObject> left = sample
            .getDataObjects()
            .stream()
            .filter(o -> node.goesLeft(o, splitter.getContractor()))
            .collect(Collectors.toList());
        
        List<DataObject> right = sample
            .getDataObjects()
            .stream()
            .filter(o -> node.goesRight(o, splitter.getContractor()))
            .collect(Collectors.toList());
        
        left.addAll(missing);
        right.addAll(missing);
        
        ContractBuilder builder = new ContractBuilder();
                    
        newAttributes.forEach(a -> builder
            .addContractor(
                a,
                sample
                    .getContract()
                    .getAttributeContract(a)
            ));
            
        Contract newContract = builder.build();
        
        node.setLeftChild(split(wrap(newContract, left, newAttributes), newAttributes, splitsLeft-1));
        node.setRightChild(split(wrap(newContract, right, newAttributes), newAttributes, splitsLeft-1));
        
        return node;
    }
    
    private ContractedDataset wrap(Contract contract, List<DataObject> objects, Collection<String> attributes)
    {
        ContractedDataset sample = new ContractedDataset(contract);
        
        for(DataObject o: objects)
        {
            sample.addDataObject(o.project(attributes));
        }
        
        return sample;
    }
    
    protected String selectSplitAttribute(List<String> attributes)
    {       
        //Pick the first attribute from the shuffled list
        return attributes.get(generator.nextInt(attributes.size()));
    }
    
}
