package be.ugent.ledc.dino.outlierdetection.zscore;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.dino.OutlierDetector;
import be.ugent.ledc.core.statistics.Statistics;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A simple outlier detector that uses the Z-score as a tool to detect outliers.
 * The Z-score is computed, for each of the requested attributes, as (v-m)/std
 * where v is an observation, m is the population mean and std the standard deviation.
 * 
 * Z-scores with an absolute value greater than or equal to 3 are considered outliers.
 * Note that an objects is an outlier as soon as at least one of the attributes leads
 * to a Z-score meeting this constraint.
 * @author abronsel
 */
public class ZScoreOutlierDetector implements OutlierDetector<Dataset, Double>
{
    @Override
    public Map<DataObject, Double> findOutliers(Dataset dataset, Set<String> attributes)
    {
        Map<DataObject, Double> outliers = new HashMap<>();
                
        for(String a: attributes)
        {   
            List<Double> values = dataset
                .stream()
                .filter(o -> o.get(a) != null)
                .map(o -> new DataObject(o)
                        .asDouble(a)
                        .getDouble(a))
                .collect(Collectors.toList());
            
            double mean     = Statistics.mean(values);
            double stddev   = Statistics.stdDev(values);
            
            dataset
            .stream()
            .filter(o -> o.get(a) != null)
            .filter(o -> Math.abs((new DataObject(o).asDouble(a).getDouble(a) - mean) / stddev) >= 3.0)
            .forEach(o -> outliers.merge( //Merge outliers in outlier map, preserve highest Z-score
                    o,
                    Math.abs((new DataObject(o).asDouble(a).getDouble(a) - mean) / stddev),
                    Double::max)
            );
        }
        
        return outliers;
    }
    
}
