package be.ugent.ledc.dino.outlierdetection.hbos;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.datastructures.histogram.Histogram;
import be.ugent.ledc.core.datastructures.histogram.HistogramBuilder;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * A histogram builder that produces fixed-with histograms for ordinal datatypes.
 * @author abronsel
 * @param <D> The datatype for which we build histograms
 */
public class OrdinalFixedWidthHistogramBuilder<D extends Comparable<? super D>> implements HistogramBuilder<D, OrdinalContractor<D>>
{      
    @Override
    public Histogram<D> buildHistogram(Dataset dataset, int bins, String a, OrdinalContractor<D> contractor)
    {        
        Optional<D> oMin = dataset
            .stream()
            .filter(o -> o.get(a) != null)
            .map(o -> contractor
                    .getFromDataObject(o, a))
            .min(Comparator.naturalOrder());
        
        Optional<D> oMax = dataset
            .stream()
            .filter(o -> o.get(a) != null)
            .map(o -> contractor
                    .getFromDataObject(o, a))
            .max(Comparator.naturalOrder());
        
        if(!oMin.isPresent() || !oMax.isPresent())
        {
            return new OrdinalHistogram<>(contractor, new HashMap<>());
        }
           
        D min = oMin.get();
        D max = oMax.get();
            
        long diff = contractor
            .cardinality(
                new Interval<>(
                    min,
                    max,
                    false,
                    false)
                );
        
        if(diff == 0)
        {
            System.out.println("No histogram for '" + a + "': all data points are equal.");
            return null;
        }
        
        if(diff < bins)
        {
            bins = (int)diff;
        }
            
        //Compute bin width
        long w = diff / bins;
                    
        Map<Interval<D>, Long> binningMap = new LinkedHashMap<>();
            
        for(long i=0; i<bins-1; i++)
        {
            binningMap
                .put(
                    new Interval<>(
                        contractor.add(min, i*w),
                        contractor.add(min, ((i+1)*w)),
                        false, //Include left bound
                        true), //Exclude right bound
                    0l);
        }

        binningMap
            .put(
                new Interval<>(
                    contractor.add(min, (int)(w*(bins-1))),
                    max,
                    false,
                    false),
                0l);

        OrdinalHistogram<D> ordinalHistogram = new OrdinalHistogram<>(contractor, binningMap);
        ordinalHistogram.fill(dataset, a, contractor);
        return ordinalHistogram;
    }
}
