package be.ugent.ledc.dino.outlierdetection;

import java.util.Set;

public abstract class AbstractNeighbourSearch implements NeighbourSearch
{    
    private final Set<String> attributes;

    public AbstractNeighbourSearch(Set<String> attributes)
    {
        this.attributes = attributes;
    }

    public Set<String> getAttributes()
    {
        return attributes;
    }
}
