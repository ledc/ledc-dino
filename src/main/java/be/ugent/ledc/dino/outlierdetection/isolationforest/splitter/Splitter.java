package be.ugent.ledc.dino.outlierdetection.isolationforest.splitter;

import be.ugent.ledc.core.dataset.ContractedDataset;

public interface Splitter<T extends Comparable<T>>
{
    public T selectRandomSplit(ContractedDataset sample, String splitAttribute);
}
