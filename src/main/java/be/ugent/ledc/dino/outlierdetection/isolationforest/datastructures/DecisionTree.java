package be.ugent.ledc.dino.outlierdetection.isolationforest.datastructures;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.datastructures.Interval;
import java.util.HashMap;
import java.util.Map;

public class DecisionTree<T extends Comparable<T>>
{
    private final DecisionNode<T> rootNode;
    
    private final int maximalDepth;
    
    private final TypeContractor<T> contractor;

    public DecisionTree(DecisionNode<T> rootNode, int maximalDepth, TypeContractor<T> contractor)
    {
        this.rootNode = rootNode;
        this.maximalDepth = maximalDepth;
        this.contractor = contractor;
    }
    
    public DecisionNode<T> getRootNode()
    {
        return rootNode;
    }

    public int getMaximalDepth()
    {
        return maximalDepth;
    }
    
    public double calculateAdjustedDepth(DataObject object)
    {
        return depth(rootNode, object);
    }
    
    public Map<String, Interval<T>> showPath(DataObject o)
    {
        Map<String, Interval<T>> path = new HashMap<>();
        
        if(hasPath(rootNode, o))
        {
            buildPath(rootNode, o, path);
        }
        return path;
    }
    
    private boolean hasPath(DecisionNode<T> node, DataObject o)
    {
        if(node == null || node.getAttribute() == null)
            return true;
        
        return o.get(node.getAttribute()) != null
            && (node.goesLeft(o, contractor)
                ? hasPath(node.getLeftChild(), o)
                : hasPath(node.getRightChild(), o));
    }
    
    private void buildPath(DecisionNode<T> node, DataObject o, Map<String, Interval<T>> path)
    {
        String attribute = node.getAttribute();
        
        if(attribute == null)
            return;
        
        //Init the condition on the attribute
        Interval<T> condition = path.get(attribute) == null ? new Interval<>(null,null) : path.get(attribute);

        if(node.goesLeft(o, contractor))
        {
            path.put(attribute, condition.intersect(new Interval<>(null, node.getThreshold(), true, false)));
            buildPath(node.getLeftChild(), o, path);
        }
        else
        {
            path.put(attribute, condition.intersect(new Interval<>(node.getThreshold(), null)));
            buildPath(node.getRightChild(), o, path);
        }
    }
    
    private double depth(DecisionNode<T> node, DataObject o)
    {
        if(node.getLeftChild() == null && node.getRightChild() == null)
            return IsolationForest.meanTreeDepth(node.getSamples());
        
        if(o.get(node.getAttribute()) == null)
            return 1.0 + Math.min(depth(node.getLeftChild(), o), depth(node.getRightChild(),o));
        else if(node.goesLeft(o, contractor))
            return 1.0 + depth(node.getLeftChild(), o);
        else
            return 1.0 + depth(node.getRightChild(), o);
    }
}
