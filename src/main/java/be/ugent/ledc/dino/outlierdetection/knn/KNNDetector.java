package be.ugent.ledc.dino.outlierdetection.knn;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import be.ugent.ledc.core.operators.metric.Metric;
import be.ugent.ledc.dino.OutlierDetector;
import be.ugent.ledc.dino.outlierdetection.NeighbourSearch;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * An outlier detector that computes an outlier score based on the distances of a point
 * to it's k nearest neighbors. The exact mapping of this set of distances to a score
 * can differ and this is encoded in the KNNProcessor. Default behavior is to take
 * the maximal distances as the score.
 * @author abronsel
 */
public class KNNDetector implements OutlierDetector<Dataset, Double>
{
    private final NeighbourSearch neighbourSearch;
    
    private final int k;
    
    private final double threshold;
    
    private final KNNProcessor processor;
    
    private final Metric<Double, DataObject> metric;
    
    public static boolean SHOW_PROGRESS = false;

    public KNNDetector(NeighbourSearch neighbourSearch, int k, double threshold, KNNProcessor processor, Metric<Double, DataObject> metric)
    {
        this.neighbourSearch = neighbourSearch;
        this.k = k;
        this.threshold = threshold;
        this.processor = processor;
        this.metric = metric;
    }
    
    public KNNDetector(NeighbourSearch neighbourSearch, int k, double threshold, Metric<Double, DataObject> metric)
    {
        this(neighbourSearch, k, threshold, KNNProcessor.MAX, metric);
    }
    
    public KNNDetector(NeighbourSearch neighbourSearch, int k, double threshold)
    {
        this(neighbourSearch, k, threshold, KNNProcessor.MAX, DataObjectMetrics.EUCLIDEAN);
    }
    
    @Override
    public Map<DataObject, Double> findOutliers(Dataset dataset, Set<String> attributes)
    {
        Map<DataObject, Double> outliers = new HashMap<>();
        int count = 0;
        int steps = 0;
        for(DataObject o: dataset)
        {
            Double score = processor
                .apply(
                    neighbourSearch
                        .getKNearestNeighbours(
                            o,
                            k,
                            metric)
                        .stream()
                        .mapToDouble(nn -> metric
                            .distance(
                                o.project(attributes),
                                nn.project(attributes))
                        )
                );
            
            if(score > threshold)               
                outliers.put(o, score);
            
            count++;
            if(SHOW_PROGRESS && (count*10/dataset.getSize()) > steps)
            {
                steps++;
                String progress = "";
                
                for(int i=1;i<=10;i++)
                {
                    progress += i<=steps ? "|": " ";
                }
                
                System.out.println("[" + progress + "]");
            }
            
        }
        
        return outliers;
    }
}
