package be.ugent.ledc.dino.outlierdetection.lof;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import be.ugent.ledc.core.operators.metric.Metric;
import be.ugent.ledc.dino.OutlierDetector;
import be.ugent.ledc.dino.outlierdetection.NeighbourSearch;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of the LOF (Local Outlier Factor) algorithm that makes a local
 * estimation of density and uses that estimate to compute an outlier score.
 * @author abronsel
 */
public class LOFDetector implements OutlierDetector<Dataset, Double>
{
    private final NeighbourSearch neighbourSearch;
    
    private final int k;
    
    private final double threshold; 
    
    private final Metric<Double, DataObject> metric;
    
    private final boolean simplifiedLof;
    
    public LOFDetector(NeighbourSearch neighbourSearch, int k, double threshold, Metric<Double, DataObject> metric, boolean simplifiedLof)
    {
        this.neighbourSearch = neighbourSearch;
        this.k = k;
        this.threshold = threshold;
        this.metric = metric;
        this.simplifiedLof = simplifiedLof;
    }
    
    public LOFDetector(NeighbourSearch neighbourSearch, int k, double threshold)
    {
        this(neighbourSearch, k, threshold, DataObjectMetrics.EUCLIDEAN, false);
    }

    @Override
    public Map<DataObject, Double> findOutliers(Dataset dataset, Set<String> attributes)
    {
        
        //Map to store k-distances
        Map<DataObject, Double> kDistances = new HashMap<>();
        
        //Map to store neighborhoods
        Map<DataObject, List<DataObject>> neighborHood = new HashMap<>();
        
        System.out.println("Computing local neighborhoods...");
        //Compute neighborhoods and radii
        for(DataObject o: dataset)
        {
            //Get the distance to the k-th neighbour
            double maxDistance = neighbourSearch
                .getKNearestNeighbours(o, k, metric)
                .stream()
                .mapToDouble(nn -> metric
                    .distance(
                        o.project(attributes),
                        nn.project(attributes)))
                .max()
                .getAsDouble();
            
            //Store distances to farest neighbour
            kDistances.put(o, maxDistance);
            
            //Store distances to neighbors.
            neighborHood.put(o, neighbourSearch.getNeighbourhood(o, maxDistance, metric));
        }
        
        Map<DataObject, Double> lrds = new HashMap<>();
        
        System.out.println("Computing local densities...");
        
        //Compute Local Reachability Densities
        for(DataObject o: dataset)
        {
            double avgReachDist =
                neighborHood
                .get(o)
                .stream()
                .mapToDouble(nn -> 
                    simplifiedLof
                    ? kDistances.get(nn)
                    : Math.max(kDistances.get(nn), metric.distance(o.project(attributes), nn.project(attributes))))
                .average()
                .getAsDouble();
            
            lrds.put(o, 1.0 / avgReachDist);
        }
        
        Map<DataObject, Double> outliers = new HashMap<>();
        
        System.out.println("Computing LOF scores...");
        
        //Compute Local Outlier Factor
        for(DataObject o: dataset)
        {
            //Now we compute the LOF score as the average ratio of the density of neigbours over density of the point
            double lof =
                neighborHood
                .get(o)
                .stream()
                .mapToDouble(nn -> lrds.get(nn) / lrds.get(o))
                .average()
                .getAsDouble();
            
            if(lof > threshold)
                outliers.put(o, lof);
        }
        
        return outliers;
    }
    
}
