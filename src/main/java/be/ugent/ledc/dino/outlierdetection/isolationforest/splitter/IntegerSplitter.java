package be.ugent.ledc.dino.outlierdetection.isolationforest.splitter;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import java.util.Random;

public class IntegerSplitter extends AbstractSplitter<Integer>
{
    private final Random generator = new Random(System.nanoTime());

    public IntegerSplitter()
    {
        super(TypeContractorFactory.INTEGER);
    }
    
    @Override
    public Integer selectRandomSplit(ContractedDataset sample, String splitAttribute)
    {
        Integer min = sample
            .getDataObjects()
            .stream()
            .filter(o -> o.get(splitAttribute) != null)
            .mapToInt(o -> getContractor().getFromDataObject(o, splitAttribute))
            .min()
            .getAsInt();
        
        Integer max = sample
            .getDataObjects()
            .stream()
            .filter(o -> o.get(splitAttribute) != null)
            .mapToInt(o -> getContractor().getFromDataObject(o, splitAttribute))
            .max()
            .getAsInt();
        
        return max.equals(min) ? min : min + generator.nextInt(max - min);
    }
}
