package be.ugent.ledc.dino.outlierdetection.isolationforest.splitter;

import be.ugent.ledc.core.dataset.contractors.TypeContractor;

public abstract class AbstractSplitter<T extends Comparable<T>> implements Splitter<T>
{
    private final TypeContractor<T> contractor;

    public AbstractSplitter(TypeContractor<T> contractor)
    {
        this.contractor = contractor;
    }

    public TypeContractor<T> getContractor()
    {
        return contractor;
    }
}
