package be.ugent.ledc.dino.outlierdetection.hbos;

import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.dino.OutlierDetector;
import be.ugent.ledc.core.datastructures.histogram.CategoricalHistogramBuilder;
import be.ugent.ledc.core.datastructures.histogram.Histogram;
import be.ugent.ledc.core.datastructures.histogram.HistogramBuilder;
import be.ugent.ledc.core.datastructures.histogram.RealDynamicHistogramBuilder;
import be.ugent.ledc.core.datastructures.histogram.RealFixedWidthHistogramBuilder;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HBOSDetector implements OutlierDetector<ContractedDataset, Double>
{
    private final boolean dynamicBinning;
    
    private final double threshold;

    public HBOSDetector(double threshold, boolean dynamicBinning)
    {
        this.dynamicBinning = dynamicBinning;
        this.threshold = threshold;
    }
    
    public HBOSDetector(double threshold)
    {
        this(threshold, true);
    }
    
    @Override
    public Map<DataObject, Double> findOutliers(ContractedDataset dataset, Set<String> attributes)
    {
        Map<DataObject, Double> outlierMap = new HashMap<>();
        
        //Step 1: compute histograms for each attribute
        Map<String, Histogram> histograms = new HashMap<>();
        
        int bins = (int) Math.ceil(Math.sqrt(dataset.getSize()));
        
        for(String a: attributes)
        {
            TypeContractor contractor = dataset
                .getContract()
                .getAttributeContract(a);
            
            //Categorical case
            if(contractor.equals(TypeContractorFactory.STRING))
            {
                histograms.put(
                    a,
                    new CategoricalHistogramBuilder()
                        .buildHistogram(
                            dataset,
                            bins,
                            a,
                            contractor)
                );
            }
            else if(contractor.equals(TypeContractorFactory.DOUBLE))
            {
                HistogramBuilder<Double, TypeContractor<Double>> builder =
                    this.dynamicBinning
                        ? new RealDynamicHistogramBuilder()
                        : new RealFixedWidthHistogramBuilder();
                
                histograms.put(
                    a,
                    builder.buildHistogram(
                        dataset,
                        bins,
                        a,
                        TypeContractorFactory.DOUBLE)
                );
            }
            else if(contractor instanceof OrdinalContractor)
            {
                HistogramBuilder<?, OrdinalContractor<?>> builder =
                    this.dynamicBinning
                        ? new OrdinalDynamicHistogramBuilder()
                        : new OrdinalFixedWidthHistogramBuilder();
                
                histograms.put(
                    a,
                    builder.buildHistogram(
                        dataset,
                        bins,
                        a,
                        (OrdinalContractor<?>) contractor)
                );
            }
            else
                throw new RuntimeException("Could not compute a histogram for attribute '"
                    + a
                    + "'. Cause: unsupported contractor.");
        }
        
        for(DataObject o: dataset)
        {
            double outlierScore = attributes
                .stream()
                .filter(a -> o.get(a) != null)
                .map(a -> histograms //Map to normalized bin height
                    .get(a)
                    .normalizedBinHeight((Comparable<? super Object>) o.get(a)))
                .mapToDouble(d -> Math.log(1/d)) //Map to log of inverse
                .sum();
                        
            if(outlierScore > threshold)
                outlierMap.put(o, outlierScore);
        }
        
        return outlierMap;
    }
    
}
