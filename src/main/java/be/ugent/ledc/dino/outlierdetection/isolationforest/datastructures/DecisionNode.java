package be.ugent.ledc.dino.outlierdetection.isolationforest.datastructures;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractor;

public class DecisionNode<T extends Comparable<T>>
{
    private final String attribute;
    
    private final T threshold;
    
    private final int samples;
    
    private DecisionNode<T> leftChild;
    
    private DecisionNode<T> rightChild;

    public DecisionNode(String attribute, T threshold, int samples)
    {
        this(attribute, threshold, samples, null, null);
    }

    public DecisionNode(String attribute, T threshold, int samples, DecisionNode<T> leftChild, DecisionNode<T> rightChild)
    {
        this.attribute = attribute;
        this.threshold = threshold;
        this.samples = samples;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public boolean goesLeft(DataObject object, TypeContractor<T> contractor)
    {        
        return object.get(attribute) != null && contractor.getFromDataObject(object, attribute).compareTo(threshold) <= 0;
    }
    
    public boolean goesRight(DataObject object, TypeContractor<T> contractor)
    {        
        return object.get(attribute) != null && contractor.getFromDataObject(object, attribute).compareTo(threshold) > 0;
    }
    
    public String getAttribute()
    {
        return attribute;
    }

    public int getSamples()
    {
        return samples;
    }

    public T getThreshold()
    {
        return threshold;
    }

    public DecisionNode<T> getLeftChild()
    {
        return leftChild;
    }

    public void setLeftChild(DecisionNode<T> leftChild)
    {
        this.leftChild = leftChild;
    }

    public DecisionNode<T> getRightChild()
    {
        return rightChild;
    }

    public void setRightChild(DecisionNode<T> rightChild)
    {
        this.rightChild = rightChild;
    }

    @Override
    public String toString()
    {
        return (attribute == null) ? "LEAF" : attribute + "<=" + threshold + " (" + samples + ")";
    }
    
    
}
