# Overview

The ledc-dino module basically contains two principle interfaces (`OutlierDetector` and `RuleMiner`) that both aim at exposing potential artifacts in a dataset.
Instances of these interfaces differ in the way they identify potential artifacts.

* A **RuleMiner** accepts a [dataset](https://gitlab.com/ledc/ledc-core/-/blob/master/docs/data-model.md#dataset) and returns a set of [sigma rules](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md) that can serve as constraints to which the given Dataset should obey.

* An **OutlierDetector** accepts a [dataset](https://gitlab.com/ledc/ledc-core/-/blob/master/docs/data-model.md#dataset) and inspects a given set of attributes with an outlier detection algorithm.
The result of this inspection, is a collection of [data objects](https://gitlab.com/ledc/ledc-core/-/blob/master/docs/data-model.md#dataobject) paired with an outlier score.

For both interfaces, we provide a few implementations that are summarized below.
- [Association-based mining](#association-based-mining)
- [Lift-based mining](#lift-based-mining)
- [Monotone mining](#monotone-mining)
- [z-score](#z-score)
- [k-nearest neighbour](#k-nearest-neighbour)
- [DBSCAN](#dbscan)
- [Isolation forest](#isoliation-forest)
- [Singular outliers](#singular-outliers)
- [Histogram based outlier detection](#histogram-based-outlier-detection)

# Rule miners

The premier goal of ledc-dino is to make some new error detection algorithms available.
These algorithms inspect a dataset and compose a set of constraints to which the data should adhere.
The constraints are then formulated as [sigma rules](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md), which can then be passed on to a [repair engine](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/repair.md).
Below we list three algorithms to find sigma rules with [constant atoms](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md#atoms).
Example code illustrates their usage with the [Adult](https://archive.ics.uci.edu/ml/datasets/adult) dataset of which a copy can be found in the [data folder](../data).

## Association-based mining

**Basic idea** The first method to find rules is to search for extremely high confidence association rules.
Such rules are then converted to sigma rules by application of the rules of logic.

**Restrictions** Association-based mining can be applied solely on attributes of the string type.

**Parameters** The association-based mining approach to find rules has four parameters that must be specified (two of them have a default value)

- The parameter alpha sets the lower bound for the support of value combinations. Must be between 0 and 1.
- The parameter beta sets the lower bound for the confidence of value combinations. Must be between 0 and 1, but empirical results show that values between 0.98 and 1 give the best result. Values below 0.5 are pointless.
- The parameter sigma allows for additional filtering of high confidence rules. Must be chosen equal to or greater than 1. Default value is 1 in which no filtering occurs. The sigma test utilizes a generalization of the lift measure to filter rules.
- The parameter epsilon allows for additional filtering of high confidence rules. Must be chosen between 0 and 1. Default value is 1 in which no filtering occurs. The epsilon test prevents very low frequence values to be pushed out of the dataset by high frequence values.

**Example** The code below shows how to find rules by using assocation-based mining on a selection of categorical attributes of the Adult dataset.
An example rule obtained with this code is `relationship == 'Husband' & marital-status != 'Married-civ-spouse'`, which is derived from the association rule `IF relationship == 'Husband' THEN marital-status == 'Married-civ-spouse'`.

```java
//CSV Binder with header line, comma-separator and ? as null symbol
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())
    ),
    new File("data/adult.csv"));

//Read the data
FixedTypeDataset<String> dataset = new CSVDataReader(csvBinder).readData();

SigmaRuleset rules = new ConfidenceMiner
    .ConfidenceMinerBuilder(0.3, 0.98) //Pass alpha (support threshold) and beta (confidence threshold)
    .withEpsilon(0.9) //Optional epsilon parameter to avoid low-frequent values being "pushed out" by high frequent ones
    .withSigma(1.1)   //Optional sigma parameters which is generalization of lift.
    .build()
    .findRules(dataset,
        "workclass",
        "education",
        "marital-status",
        "occupation",
        "relationship",
        "sex",
        "native-country"
    );

rules
.stream()
.filter(rule -> rule.getInvolvedAttributes().size() >1)
.forEach(System.out::println);
```

## Lift-based mining

**Basic idea** Lift is a concept from association analysis that can be used to measure the strength of correlation between variables.
In the lift-based mining approach, a combination of values is considered erroneous if there is a *very strong negative correlation* between those values.
In other words, this approach will emit rules for any combination of values for which the measured lift is below a predefined threshold $`\tau`$.

**Restrictions** Lift-based mining can be applied solely on attributes of the string type.

**Parameters** The lift-based mining approach to find rules has two parameters that must be specified.

- The first parameter is a [triangular norm](https://en.wikipedia.org/wiki/T-norm) $`T`$ that must be used to model independence.
The usual statistical (in)dependence is obtained when $`T=T_P`$ (i.e., the product norm), but other operators can be used.
For example, the minimum norm and the Hamacher product norm are two alternatives worth trying.
When selecting an operator, keep in mind that using a pointwise *smaller* $`T`$ results in a *stronger* notion of independence and thus implies *less* patterns to be considered as erroneous.
Besides some predefined operators in the enum `BasicTNorm`, you can use the `SchweizerSklarFactory` to create a triangular norm from the [Schweizer-Sklar family](https://en.wikipedia.org/wiki/Construction_of_t-norms#Schweizer%E2%80%93Sklar_t-norms).

- The second parameter is $`\tau`$, which serves as the threshold for lift.
Combinations of values that have a lift below this threshold will be considered erroneous and will result in a sigma rule to be outputted.
Thus, *lower* values for $`\tau`$ will result in *less* patterns to be considered as erroneous, but these patterns tend to be *more* reliable.
Reasonable values for $`\tau`$ are any positive number below `0.05`, but this depends on the dataset at hand.

**Example** The code below shows how to find rules with lift-based mining on a selection of categorical attributes of the Adult dataset.
An example rule obtained with this code is `sex == 'Female' & relationship == 'Husband'`.

```java
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())),
    new File("data/adult.csv"));
        
//Read the raw data
FixedTypeDataset<String> dataset = new CSVDataReader(csvBinder).readData();
        
SigmaRuleset rules = new TGenerator
(
    0.01, //Threshold for the lift: patterns with lower lift are considered erroneous
    true, //Ignore null values in pattern computation
    BasicTNorm.PRODUCT //Triangular norm that must be used for the computation of the lift.
)
.findRules(dataset,
    "workclass",
    "education",
    "marital-status",
    "occupation",
    "relationship",
    "sex",
    "native-country"
);
        
rules
.stream()
.filter(rule -> rule.getInvolvedAttributes().size() >1)
.forEach(System.out::println);
```

## Monotone mining

**Basic idea** The rule mining approaches mentioned so far have the restriction they operate solely on nominal data.
The monotone mining approach differs in the fact it draws sigma rules from *ordinal data*.
The core idea is that it tries to find split points in the domains of two attributes such that at this split point, a monotonic trend can be observed.
Basically, the approach works by using an ordinal alternative for the lift measure from the lift-based approach.

**Restrictions** The approach works for ordinal data and requires attributes to be contracted with an `OrdinalContractor`.
Moreover, the produced sigma rules are bound to have no more than two attributes involved.

**Parameters** A cutoff parameter below which the ordinal extension of the lift must be such that a sigma rule is outputted.
*Lower* values for this parameter will result in *less* sigma rules to be produced, but they tend be *more* reliable.
Reasonable values for this cutoff parameter are any positive number below `0.05`, but this depends on the dataset at hand.

**Example** The code below shows how to find rules with monotone mining on a selection of integer-valued attributes of the Adult dataset.
An example rule obtained with this code is `education-num >= 14 & age <= 20`.
In the code below, the conversion of datatype provides an explicit contractor.
This is necessary as the code will verify if the contractors for attributes inherit from the `OrdinalContractor`.

```java
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())),
    new File("data/adult.csv"));

//Read the raw data and convert datatypes 
ContractedDataset dataset = new CSVDataReader(csvBinder)
    .readData()
    .asInteger("age", SigmaContractorFactory.INTEGER)
    .asInteger("fnlwgt", SigmaContractorFactory.INTEGER)
    .asInteger("education-num", SigmaContractorFactory.INTEGER)
    .asInteger("capital-gain", SigmaContractorFactory.INTEGER)
    .asInteger("capital-loss", SigmaContractorFactory.INTEGER)
    .asInteger("hours-per-week", SigmaContractorFactory.INTEGER);
        
SigmaRuleset rules = new OrdinalBinaryMiner(0.01)
    .findRules(
        dataset,
        "age",
        "fnlwgt",
        "education-num",
        "capital-gain",
        "capital-loss",
        "hours-per-week"    
    );

rules
.stream()
.filter(rule -> rule.getInvolvedAttributes().size() > 1)
.forEach(System.out::println);
```

# Outlier detectors

In ledc-dino, the outlier detectors mentioned below have been implemented. 
These are implementations of well-known and efficient algorithms.
Example code illustrates their usage with the [Breast-Cancer Wisconsin (BCW)](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic)) dataset of which a copy can be found in the [data folder](../data).

## Z-score

**Basic idea** The Z-score outlier detector is a simple outlier detector that uses the Z-score as the principle mechanism.
The Z-score is computed, for each of the requested attributes, as $`\frac{x-\mu}{\sigma}`$ where $`x`$ is an observation, $`\mu`$ is the population mean and $`\sigma`$ the population standard deviation.
In this implementation, we use the fixed threshold of 3 for the absolute value of the Z-score to find outliers and inspect each of the pre-determined attributes individually.
That is, an object is treated as an outlier as soon as one of the attributes features a value $`x`$ for which the Z-score exceeds 3.

**Restrictions** In the implementation of the algorithm, we allow any attribute that has a datatype that can be [converted to the double type](https://gitlab.com/ledc/ledc-core/-/blob/master/docs/data-model.md#operators).
In the Z-score is to be computed on an attribute that does not meet this constraint, a DataException will be thrown.

**Parameters** The Z-score has no parameters.

**Example** The code below shows how to find outliers with the Z-score on the BCW dataset.

```java
//CSV Binder with header line, comma-separator and ? as null symbol
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())),
    new File("data/breast-cancer-wisconsin.csv")
);

//Read dataset and use a sample of 100 rows to do type inference 
Dataset dataset = new CSVDataReader(csvBinder)
    .readDataWithTypeInference(100);

//Find outliers with the Z-score  
Map<DataObject, Double> outliers = new ZScoreOutlierDetector()
    .findOutliers(dataset,
        "clump_thickness",
        "uniformity_of_cell_size",
        "uniformity_of_cell_shape",
        "marginal_adhesion",
        "single_epithelial_cell_size",
        "bare_nuclei",
        "bland_chromatin",
        "normal_nucleoli",
        "mitoses"
    );
        
//Report
System.out.println("#objects: " + dataset.getSize());
System.out.println("#outliers: " + outliers.size());
```

## k-nearest neighbour

**Basic idea** The KNN outlier detector is a simple outlier detector that computes the distances to the k nearest neighbours of a point.
Based on those distances, we compute a final score.
This could be the maximal distance, the average distance or something else.

**Restrictions** There are no real restrictions on the actual datatypes of the attributes as long as a suitable distance function can be defined for it.
If data are numeric, the standard Euclidean distance is predefined.

**Parameters** Besides the metric mentioned just above, the KNN detector takes the following parameters.

- The parameter `neighbourSearch` determines the strategy to search in the metric space.
Currently, there is support for *brute force* searching and searching with *KD-trees*.
For datasets with high volumes, using KD trees can be far more **efficient**, but be aware of the fact that there is currently **no support
for handling of null values** with KD-trees.

- The parameter `k` determines how many neighbours are taken into account to compute the score.

- The parameter `threshold` determines the threshold for the outlier scores.
A score above the given value will result in considering the object as an outlier.


**Example** The code below shows how to find outliers with the KNN detector on the BCW dataset.

```java
//CSV Binder with header line, comma-separator and ? as null symbol
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())),
    new File("data/breast-cancer-wisconsin.csv")
);

//Read dataset and use a sample of 100 rows to do type inference 
Dataset dataset = new CSVDataReader(csvBinder)
    .readDataWithTypeInference(100);

//Attributes relevant to the computations
Set<String> attributes = Stream.of(
    "clump_thickness",
    "uniformity_of_cell_size",
    "uniformity_of_cell_shape",
    "marginal_adhesion",
    "single_epithelial_cell_size",
    "bare_nuclei",
    "bland_chromatin",
    "normal_nucleoli",
    "mitoses")
.collect(Collectors.toSet());

//Find outliers with the KNN Detector
Map<DataObject, Double> outliers = new KNNDetector(
    new BruteForceNeighbourSearch(attributes, dataset),
    5,                  //Amount of neighbours to search for
    7.0,                //Threshold for the score: above this, we call it an outlier
    KNNProcessor.MAX,   //Computation of the score based on k distances
    DataObjectMetrics.EUCLIDEAN)
.findOutliers(dataset, attributes);
        
//Report
System.out.println("#objects: " + dataset.getSize());
System.out.println("#outliers: " + outliers.size());
```

## DBSCAN

**Basic idea** [DBSCAN](https://www.aaai.org/Papers/KDD/1996/KDD96-037.pdf) (Density-based spatial clustering of applications with noise) is an algorithm for clustering that uses the notion of *density*.
It seeks to form clusters as a maximal set of points that is *dense* and *connected*.
One of the features of DBSCAN, is that some points don't belong to any such maximal cluster.
Those points are treated as noise.
Consequently, we can find outliers as those points that don't belong to any cluster.

**Restrictions** There are no real restrictions on the actual datatypes of the attributes as long as a suitable distance function can be defined for it.
If data are numeric, the standard Euclidean distance is predefined (see example below).
Other distance functions can be easily implemented if needed.

**Parameters** Besides the above mentioned distance function, DBSCAN has two parameters that must be specified.
- The parameter `neighbourSearch` determines the strategy to search in the metric space.
Currently, there is support for *brute force* searching and searching with *KD-trees*.
For datasets with high volumes, using KD trees can be far more **efficient**, but be aware of the fact that there is currently **no support
for handling of null values** with KD-trees.

- The parameter `eps` determines the "neighbourhood" of a data point.
More specifically, the neighbourhood of a point `p` is a multi-dimensional sphere with radius `eps`.

- The parameter `minPts` determines whether a point `p` is a *core* point.
More specifically, a point `p` is a *core* point if the neighbourhood of `p` contains at least `minPts` data points from the dataset.

For more information on the role of these parameters and rules-of-thumb to assign values to them, we refer to the [original article](https://www.aaai.org/Papers/KDD/1996/KDD96-037.pdf).

**Example** The code below shows how to find outliers with the DBSCAN detector on the BCW dataset.
The parameters are merely as an illustration.

```java
//CSV Binder with header line, comma-separator and ? as null symbol
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())),
    new File("data/breast-cancer-wisconsin.csv")
);

//Read dataset and use a sample of 100 rows to do type inference 
Dataset dataset = new CSVDataReader(csvBinder)
    .readDataWithTypeInference(100);
        
Set<String> attributes = Stream.of(
    "clump_thickness",
    "uniformity_of_cell_size",
    "uniformity_of_cell_shape",
    "marginal_adhesion",
    "single_epithelial_cell_size",
    "bare_nuclei",
    "bland_chromatin",
    "normal_nucleoli",
    "mitoses")
.collect(Collectors.toSet());
    
//DBScanDetector takes an explicit nearest neighbour search strategy
Map<DataObject, Boolean> outliers = new DBScanDetector(
    new BruteForceNeighbourSearch(attributes, dataset),
    6.0,
    10)
.findOutliers(dataset, attributes);
        
System.out.println("#objects: " + dataset.getSize());
System.out.println("#outliers: " + outliers.size());
```
## 

**Basic idea** [Isolation forest](https://cs.nju.edu.cn/zhouzh/zhouzh.files/publication/icdm08b.pdf?q=isolation-forest) is an outlier detection algorithm that grows a set of isolation trees.
Hereby, an isolation tree is a decision tree that is grown by random splits in the datasets.
Splitting stops if a single point remains and that point is then said to be *isolated*.
The idea of isolation forest is that outliers will on average be isolated faster than regular data points.

**Restrictions** There is no restriction on the [datatype](https://gitlab.com/ledc/ledc-core/-/blob/master/docs/data-model.md#datatypes) of attributes, but the current implementation requires a fix strategy for splitting, which implies that the attributes on which the isolation trees are grown, must be of the same datatype.

**Parameters** Besides the splitting strategy, the isolation forest detector takes two parameters:

- The number of trees that must be grown in the forest. Default value is 100.

- The sample size of each subsample to grow a tree on. Default value is 256.

**Example** The code below shows how to find outliers with the IsolationForest detector on the BCW dataset.
The parameters are merely as an illustration.

```java
//CSV Binder with header line, comma-separator and ? as null symbol
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())),
    new File("data/breast-cancer-wisconsin.csv")
);

//Read dataset and use a sample of 100 rows to do type inference 
Dataset dataset = new CSVDataReader(csvBinder)
    .readDataWithTypeInference(100);

Map<DataObject, Double> outliers = new IsolationForestDetector(
    new IntegerSplitter(),
    0.6, //Threshold for outlier score
    100, //Number of trees
    256 //Sample size
)
.findOutliers(dataset,
    "clump_thickness",
    "uniformity_of_cell_size",
    "uniformity_of_cell_shape",
    "marginal_adhesion",
    "single_epithelial_cell_size",
    "bare_nuclei",
    "bland_chromatin",
    "normal_nucleoli",
    "mitoses"
);
   
System.out.println("#objects: " + dataset.getSize());
System.out.println("#outliers: " + outliers.size());
```
## Singular outliers

**Basic idea** The Soda detector tries to identify objects that divert from it's `k` nearest neighbours and in addition this diversion can be explained by only a small amount of attributes.
It was originally published [here](https://link.springer.com/chapter/10.1007/978-3-319-91479-4_41) and the rationale of this detector is that in some cases like financial fraud or swap errors in data, an object can look quite normal except for a few unusual attribute values.
The Soda detector identifies outliers by computing the *Local Euclidean Manhattan Ratio* (LEMR) for each object and than compares it to a threshold.

**Restrictions** Attributes involved in the outlier computation must be numeric.

**Parameters** The Soda detector takes the following parameters.

- The parameter `neighbourSearch` determines the strategy to search in the metric space.
Currently, there is support for *brute force* searching and searching with *KD-trees*.
For datasets with high volumes, using KD trees can be far more **efficient**, but be aware of the fact that there is currently **no support
for handling of null values** with KD-trees.

- The parameter `k` determines how many neighbours are taken into account to compute the score.

- The parameter `threshold` determines the threshold for the outlier scores.
A score above the given value will result in considering the object as an outlier.

**Example** The code below shows how to find outliers with the Soda detector on the BCW dataset.

```java
//CSV Binder with header line, comma-separator and ? as null symbol
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())),
    new File("data/breast-cancer-wisconsin.csv")
);

//Read dataset and use a sample of 100 rows to do type inference 
Dataset dataset = new CSVDataReader(csvBinder)
    .readDataWithTypeInference(100);

//Attributes relevant to the computations
Set<String> attributes = Stream.of(
    "clump_thickness",
    "uniformity_of_cell_size",
    "uniformity_of_cell_shape",
    "marginal_adhesion",
    "single_epithelial_cell_size",
    "bare_nuclei",
    "bland_chromatin",
    "normal_nucleoli",
    "mitoses")
.collect(Collectors.toSet());

//Find outliers with the SodaDetector
Map<DataObject, Double> outliers = new SodaDetector(
    new BruteForceNeighbourSearch(attributes, dataset),
    10,
    0.8
)
.findOutliers(dataset, attributes);
        
//Report
System.out.println("#objects: " + dataset.getSize());
System.out.println("#outliers: " + outliers.size());
```
## Histogram based outlier detection

**Basic idea** The HBOS detector is a fast and simple algorithm to compute outliers.
It was originally published [here](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.401.5686&rep=rep1&type=pdf) and the rationale of this detector is that it can be computed much faster than other scores.
The main downside might be that there is no accounting for interactions between attributes.
For each attribute, the HBOS detector computes a histogram and it uses the height of the histogram bar (this is the number of observations divided by the histogram width) to compute a score.
More specifically, for each attribute `a` in an object, it takes the `log` of the *inverse* of the *normalized* bin height of the bin in which the value resides.
The final score is the sum over all attributes.
Some notes to take into account:

1. The number of bins in a histogram is computed as $`\lceil\sqrt{|D|}\rceil`$ for a dataset $`D`$.

2. To support [ordinal datatypes](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/sigma-rules.md#contractors) like integers, dates and timestamps in a generic way, it's important to assign an OrdinalContractor to ordinal attributes in the dataset (see example below).

**Restrictions** No restrictions on the datatypes.

**Parameters** The HBOS detector takes the following parameters.
- The parameter `threshold` determines the threshold for the outlier scores.
A score above the given value will result in considering the object as an outlier.

- The parameter `dynamicBinning` determines if bin width should be computed dynamically.
When set to true, the widths of the bins are computed such that each bin contains the same amount of observations.
Else, bin widths are the same for each bin.

**Example** The code below shows how to find outliers with the HBOS detector on the BCW dataset.

```java
//CSV Binder with header line, comma-separator and ? as null symbol
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true,
        ",",
        null,
        Stream.of("?").collect(Collectors.toSet())),
    new File("data/breast-cancer-wisconsin.csv")
);

//Attributes relevant to the computations
Set<String> attributes = Stream.of(
    "clump_thickness",
    "uniformity_of_cell_size",
    "uniformity_of_cell_shape",
    "marginal_adhesion",
    "single_epithelial_cell_size",
    "bare_nuclei",
    "bland_chromatin",
    "normal_nucleoli",
    "mitoses")
.collect(Collectors.toSet());

//Read dataset, but assign a different contractor to the integer attributes 
ContractedDataset dataset = new CSVDataReader(csvBinder)
    .readDataWithTypeInference(100)
    .asInteger(attributes, SigmaContractorFactory.INTEGER);

//Find outliers with the HBOS detector: threshold = 5 and we use dynamic binning
Map<DataObject, Double> outliers = new HBOSDetector(5, true)
    .findOutliers(dataset, attributes);
        
//Report
System.out.println("#objects: " + dataset.getSize());
System.out.println("#outliers: " + outliers.size());
```
